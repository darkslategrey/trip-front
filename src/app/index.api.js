(function ()
{
    'use strict';

    angular
        .module('fuse')
        .factory('api', apiService);

    /** @ngInject */
    function apiService(Restangular)
    {
        var api = {};

        var baseCampaign = Restangular.all('campaigns');
        api.campaign =  {
            all: baseCampaign.getList,
            get: baseCampaign.get,
            getPricesByCampaignId: function(id) {
                return Restangular.one('campaigns', id).customGET('prices');
            },
            create: baseCampaign.post,
            refresh: function(timestamps) {
                return baseCampaign.customPOST({timestamps: timestamps}, 'refresh');
            },
            delete: function(id) {
                return Restangular.one('campaigns', id).remove();
            },
            cancel: function(id) {
                return Restangular.one('campaigns', id).customGET('cancel');
            },
            update: baseCampaign.put
        };
        var baseVeille = Restangular.all('veilles');
        api.veille = {
            all:    baseVeille.getList,
            create: baseVeille.post,
            delete: function(id) {
                return Restangular.one('veilles', id).remove();
            },
            update: function(params) {
                var id = params.id;
                delete params.id;
                return Restangular.one('veilles', id).patch(params);

            }
        };
        var baseProxies = Restangular.all('proxies');
        api.proxy = {
            all:    baseProxies.getList,
            create: baseProxies.post,
            delete: function(id) {
                return Restangular.one('proxies', id).remove();
            },
            update: function(params) {
                var id = params.id;
                delete params.id;
                return Restangular.one('proxies', id).patch(params);
            },
            updateAll: function(proxies) {
                proxies = { proxies: proxies };
                return baseProxies.customPOST(proxies, 'update_all');
            }
        };

        var baseNotif = Restangular.all('notifs');
        api.notif =  {
            all: baseNotif.getList,
            get: baseNotif.get,
            create: baseNotif.post,
            delete: baseNotif.post,
            update: baseNotif.put
        };


        return api;
    }
})();
