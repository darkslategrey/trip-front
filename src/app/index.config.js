(function ()
{
    'use strict';

    angular
        .module('fuse')
        .config(config);

    /** @ngInject */
    function config(RestangularProvider, envServiceProvider)
    {
        // $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        /// ENV

        envServiceProvider.config({
            domains: {
                development: ['localhost', 'dev.local'],
                production: ['164.132.231.22'],
                preprod: ['astotel.blset.com'],
                localprod:['prod.local:2300']
            },
            vars: {
                development: {
                    apiUrl: 'http://localhost:5100/api'
                },
                preprod: {
                    apiUrl: 'http://astotel.blset.com/api'
                },
                localprod: {
                    apiUrl: '//prod.local:2300/api'
                },
                production: {
                    apiUrl: '//164.132.231.22/api',
                    staticUrl: '//static.acme.com'
                }
            }
        });
        envServiceProvider.check();
        var environment = envServiceProvider.get();
        var apiUrl      = envServiceProvider.read('apiUrl');
        console.log('env <' + environment + '> apiUrl <' + apiUrl + '>');

        RestangularProvider.setBaseUrl(apiUrl);

        // RestangularProvider.setBaseUrl('/api');
        // RestangularProvider.setExtraFields(['name']);
        // RestangularProvider.setResponseExtractor(function(response, operation) {
        //     return response.data;
        // });

        // RestangularProvider.addElementTransformer('accounts', false, function(element) {
        //     element.accountName = 'Changed';
        //     return element;
        // });

        // RestangularProvider.setDefaultHttpFields({cache: true});
        // RestangularProvider.setMethodOverriders(["put", "patch"]);

        // In this case we are mapping the id of each element to the _id field.
        // We also change the Restangular route.
        // The default value for parentResource remains the same.
        RestangularProvider.setRestangularFields({
            id: "id",
            route: "restangularRoute", // ???
            selfLink: "self.href"
        });

        // RestangularProvider.setRequestSuffix('.json');

        // Use Request interceptor
        // RestangularProvider.setRequestInterceptor(function(element, operation, route, url) {
        //     delete element.name;
        //     return element;
        // });

        // ..or use the full request interceptor, setRequestInterceptor's more powerful brother!
        // RestangularProvider.setFullRequestInterceptor(function(element, operation, route, url, headers, params, httpConfig) {
        //     delete element.name;
        //     return {
        //         element: element,
        //         params: _.extend(params, {single: true}),
        //         headers: headers,
        //         httpConfig: httpConfig
        //     };
        // });
    }

})();
