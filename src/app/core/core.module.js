(function ()
{
    'use strict';

    angular
        .module('app.core', [
            'ngCookies',
            'ngResource',
            'ngMaterial',
            'ui.router',
            'ngMessages'
        ]);
})();
