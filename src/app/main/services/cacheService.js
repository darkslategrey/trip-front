(function () {

    'use strict';


    angular.module('app.cacheservice').factory('veillesCache',
                                               VeillesCache);

    /** @ngInject */
    function VeillesCache($cacheFactory, api, modelTransformer, Veille,
                            $controller, $rootScope) {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', {$scope: $scope});
        $scope.name = 'VeillesCache';

        var cache        = $cacheFactory('veillesCache');

        var successAll = function(response) {
            var result = [];
            angular.forEach(response, function (veille) {
                result.push(modelTransformer.transform(veille, Veille));
            });
            cache.put('all', result);
            // $scope.traceDebug('result ' + JSON.stringify(result));
            return result;
        };

        var errorAll = function(response) {
            $scope.traceDebug('error ' + JSON.stringify(response.data));
            return response.data;
        };

        var reset = function() {
            cache.put('all', []);
        };

        var all = function() {
            var allVeilles = [];
            $scope.traceDebug('cache all call');
            if(angular.isUndefined(cache.get('all')) || cache.get('all').length == 0) {
                $scope.traceDebug('query api all');
                return api.veille.all().then(successAll, errorAll);
            } else {
                $scope.traceDebug('get all from cache');
                allVeilles = cache.get('all');
            }
            return allVeilles;
        };

        return {
            all: all,
            reset: reset
        };
    }


    angular.module('app.cacheservice').factory('proxiesCache',
                                               ProxiesCache);

    /** @ngInject */
    function ProxiesCache($cacheFactory, api, modelTransformer, Proxy,
                            $controller, $rootScope) {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', {$scope: $scope});
        $scope.name = 'ProxiesCache';

        var cache        = $cacheFactory('proxiesCache');

        var successAll = function(response) {
            var result = [];
            angular.forEach(response, function (proxy) {
                result.push(modelTransformer.transform(proxy, Proxy));
            });
            cache.put('all', result);
            // $scope.traceDebug('result ' + JSON.stringify(result));
            return result;
        };

        var errorAll = function(response) {
            $scope.traceDebug('error ' + JSON.stringify(response.data));
            return response.data;
        };

        var reset = function() {
            cache.put('all', []);
        };

        var all = function() {
            var allProxies = [];
            $scope.traceDebug('cache all call');
            if(angular.isUndefined(cache.get('all')) || cache.get('all').length == 0) {
                $scope.traceDebug('query api all');
                return api.proxy.all().then(successAll, errorAll);
            } else {
                $scope.traceDebug('get all from cache');
                allProxies = cache.get('all');
            }
            return allProxies;
        };

        return {
            all: all,
            reset: reset
        };
    }

    angular.module('app.cacheservice').factory('campaignsCache',
                                               CampaignsCache);


    /** @ngInject */
    function CampaignsCache($cacheFactory, api, modelTransformer, Campaign,
                            $controller, $rootScope, events) {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', {$scope: $scope});
        $scope.name = 'CampaignsCache';

        var cache        = $cacheFactory('campaignsCache');

        var successAll = function(response) {
            var result = [];
            if(response.length == 1 && _.has(response[0], 'error')) {
                $scope.traceDebug('get all campaigns error from cache');
                result = [response[0]];
            } else {
                angular.forEach(response, function (campaign) {
                    result.push(modelTransformer.transform(campaign, Campaign));
                });
                cache.put('all', result);
            }
            return result;
        };

        // not used
        var errorAll = function() {
            $scope.traceDebug('Erreur while getting campaigns');
            $scope.publish(events.message._ALL_CAMPAIGN_FAILED_);
        };

        var all = function() {
            return api.campaign.all().then(successAll, errorAll);
        };
        //     var allCampaigns = [];
        //     $scope.traceDebug('cache all call');
        //     if(angular.isUndefined(cache.get('all'))) {
        //         $scope.traceDebug('query api all');
            
        //     } else {
        //         $scope.traceDebug('get all from cache');
        //         allCampaigns = cache.get('all');
        //     }
        //     return allCampaigns;
        // };

        return {
            all: all
        };
    }

}());
