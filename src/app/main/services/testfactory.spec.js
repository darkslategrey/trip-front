describe('testfactory', function() {

    beforeEach(module('app'));

    var testfactory;
    beforeEach(inject(function(_testFactory_) {
        // testfactory = $injector.get('testfactory');
        testfactory = _testFactory_;
    }));

    it('should be defined', function() {
        expect(testfactory).toBeDefined(); 
    });
});
