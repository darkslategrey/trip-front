// (function () {

//     'use strict';

//     angular.module('app.dataservices').factory('notifDataService',
//                                   NotifDataService);

//     /** @ngInject */
//     function NotifDataService(events, modelTransformer, api,
//                                  Notif, Price,
//                                  $log, $rootScope, $controller) {
//         var $scope = $rootScope.$new();
//         $controller('BaseCtrl', {$scope: $scope});

//         $scope.name = 'NotifDataService';

//         var notif = [];

//         /******************* ALL ********************/

//         var all = function() {
//             return api.notif.all().then(successHdlAll, errorHdlAll);
//         };

//         var successHdlAll = function(response) {
//             $scope.traceDebug('allNotifSuccessHandler success');
//             if (response.data.length > 0) {
//                 var result = [];
//                 angular.forEach(response.data, function (notif) {
//                     result.push(modelTransformer.transform(notif, Notif));
//                 });
//                 $scope.publish(events.message._ALL_NOTIF_COMPLETE_, [result]);
//                 // $scope.$emit('$destroy');
//             } else {
//                 errorHdlAll();
//             }
//         };

//         var errorHdlAll = function () {
//             $scope.publish(events.message._ALL_NOTIF_FAILED_);
//             $scope.publish(events.message._ADD_ERROR_MESSAGE_,
//                               ['Unable to get all notifs from server',
//                                'alert.warning']);
//             // $scope.$emit('$destroy');
//         };
//         $scope.subscribe(events.message._ALL_NOTIF_, all);

//         /******************* GET BY ID ********************/

//         var getById = function(id) {
//             return api.notif.get(id).then(successHdlId, errorHdlId);
//         };

//         var successHdlId = function(response) {
//             if (response.data) {
//                 $scope.publish(events.message._NOTIF_GET_BY_ID_COMPLETE_,
//                                [modelTransformer.transform(response.data, Notif)]);
//             }
//             else {
//                 errorHdlId();
//             }
//         };

//         var errorHdlId = function () {
//             $scope.publish(events.message._NOTIF_GET_BY_ID_FAILED_);
//             $scope.publish(events.message._ADD_ERROR_MESSAGE_,
//                            ['Unable to get notif by id from server',
//                             'alert.warning']);
//         };

//         $scope.subscribe(events.message._GET_NOTIF_BY_ID_, getById);

//         /******************* GET Prices ********************/

//         var getPrices = function(id) {
//             return api.notif.getPricesByNotifId(id).then(successGetPrices,
//                                                                errorGetPrices);
//         };

//         var successGetPrices = function(response) {
//             $scope.traceDebug("get price SUCCESS");
//             $scope.publish(events.message._GET_PRICE_BY_NOTIF_ID_COMPLETE_,
//                            [modelTransformer.transform(response, Price)]);
//         };

//         var errorGetPrices = function () {
//             $scope.publish(events.message._GET_PRICE_BY_NOTIF_ID_FAILED_);
//             $scope.publish(events.message._ADD_ERROR_MESSAGE_,
//                            ['Unable to get prices for notif by id from server',
//                             'alert.warning']);
//         };

//         $scope.subscribe(events.message._GET_PRICE_BY_NOTIF_ID_, getPrices);

//         /******************* CREATE ********************/

//         var create = function(notif) {
//             $scope.traceDebug('receive NOTIF_CREATE msg ' +
//                               JSON.stringify(notif) );
//             return api.notif.create(notif).then(successHdlCreate, errorHdlCreate);
//         };

//         var successHdlCreate = function(response) {
//             $scope.traceDebug('NOTIF_CREATE SUCCESS' +
//                               JSON.stringify(response.data));
//             if (response.data) {
//                 $scope.publish(events.message._NOTIF_CREATE_COMPLETE_,
//                                   [modelTransformer.transform(response.data,
//                                                               Notif)]);
//             }
//             else {
//                 errorHdlCreate();
//             }
//         };

//         var errorHdlCreate = function () {
//             $scope.traceDebug('NOTIF_CREATE FAILURE');
//             $scope.publish(events.message._NOTIF_CREATE_FAILED_,
//                               [modelTransformer.transform({},
//                                                           Notif)]);
//             $scope.publish(events.message._ADD_ERROR_MESSAGE_,
//                               ['Unable to create notif',
//                                'alert.warning']);
//         };

//         $scope.subscribe(events.message._NOTIF_CREATE_, create);

//         /******************* DESTROY ********************/
//         var destroy = function(notif) {
//             return api.notif.delete(notif).then(successHdlDestroy,
//                                                       errorHdlDestroy);
//         };

//         var successHdlDestroy = function(response) {
//             if (response.data) {
//                 $scope.publish(events.message._DESTROY_NOTIF_COMPLETE_,
//                                   [modelTransformer.transform(response.data,
//                                                               Notif)]);
//             }
//             else {
//                 errorHdlDestroy();
//             }
//         };

//         var errorHdlDestroy = function () {
//             $scope.publish(events.message._DESTROY_NOTIF_FAILED_);
//             $scope.publish(events.message._ADD_ERROR_MESSAGE_,
//                               ['Unable to destory notif from server',
//                                'alert.warning']);
//         };

//         $scope.subscribe(events.message._DESTROY_NOTIF_, destroy);

//         /******************* UPDATE ********************/

//         var update = function(notif) {
//             return api.notif.update(notif).then(successHdlUpdate,
//                                                       errorHdlUpdate);
//         };

//         var successHdlUpdate = function(response) {
//             if (response.data) {
//                 $scope.publish(events.message._NOTIF_UPDATE_COMPLETE_,
//                                   [modelTransformer.transform(response.data,
//                                                               Notif)]);
//             }
//             else {
//                 errorHdlUpdate();
//             }
//         };

//         var errorHdlUpdate = function () {
//             $scope.publish(events.message._NOTIF_UPDATE_FAILED_);
//             $scope.publish(events.message._ADD_ERROR_MESSAGE_,
//                               ['Unable to update notif from server',
//                                'alert.warning']);
//         };

//         $scope.subscribe(events.message._NOTIF_UPDATE_, update);


//         var init = function () {
//             $scope.traceDebug('into init notifDataSrvc subscribe ' +
//                               events.message._NOTIF_CREATE_);
//             notif = [];
//         };

//         var notifDataService = {
//             init: init,
//             all: all,
//             getById: getById,
//             create: create,
//             update: update,
//             destroy: destroy
//         };
//         return notifDataService;
//     }
// })();
