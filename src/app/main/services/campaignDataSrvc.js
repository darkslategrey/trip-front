(function () {

    'use strict';

    angular.module('app.dataservices').factory('campaignDataService',
                                               CampaignDataService);

    /** @ngInject */
    function CampaignDataService(events, modelTransformer, api,
                                 Campaign, Price,
                                 $log, $rootScope, $controller) {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', {$scope: $scope});

        $scope.name = 'CampaignDataService';

        var campaign = [];

        /******************* ALL ********************/

        var all = function() {
            $scope.traceDebug('get all campaigins');
            return api.campaign.all().then(successHdlAll, errorHdlAll);
        };

        var successHdlAll = function(response) {
            $scope.traceDebug('allCampaignSuccessHandler success ');
            $scope.traceDebug('resposne ' + JSON.stringify(response));
            var result = [];
            angular.forEach(response, function (campaign) {
                result.push(modelTransformer.transform(campaign, Campaign));
            });
            $scope.publish(events.message._ALL_CAMPAIGN_COMPLETE_, [result]);
            // $scope.$emit('$destroy');
        };

        var errorHdlAll = function () {
            $scope.publish(events.message._ALL_CAMPAIGN_FAILED_);
            $scope.publish(events.message._ADD_ERROR_MESSAGE_,
                           ['Unable to get all campaigns from server',
                            'alert.warning']);
            // $scope.$emit('$destroy');
        };
        $scope.subscribe(events.message._ALL_CAMPAIGN_, all);

        /******************* GET BY ID ********************/

        var getById = function(id) {
            return api.campaign.get(id).then(successHdlId, errorHdlId);
        };

        var successHdlId = function(response) {
            if (response.data) {
                $scope.publish(events.message._CAMPAIGN_GET_BY_ID_COMPLETE_,
                               [modelTransformer.transform(response.data, Campaign)]);
            }
            else {
                errorHdlId();
            }
        };

        var errorHdlId = function () {
            $scope.publish(events.message._CAMPAIGN_GET_BY_ID_FAILED_);
            $scope.publish(events.message._ADD_ERROR_MESSAGE_,
                           ['Unable to get campaign by id from server',
                            'alert.warning']);
        };

        $scope.subscribe(events.message._GET_CAMPAIGN_BY_ID_, getById);

        /******************* GET Prices ********************/

        var getPrices = function(id) {
            $scope.traceDebug("get prices for id " + id);
            return api.campaign.getPricesByCampaignId(id).then(successGetPrices,
                                                               errorGetPrices);
        };

        var successGetPrices = function(response) {
            // $scope.traceDebug("get price SUCCESS" + JSON.stringify(response));
            var campaignId = response.data.campaignId;
            var prices = response.data.releve;
            // $scope.traceDebug("prices " + JSON.stringify(prices));
            $scope.traceDebug("campaignId " + JSON.stringify(campaignId));
            $scope.publish(events.message._GET_PRICE_BY_CAMPAIGN_ID_COMPLETE_,
                           [{campaignId: campaignId,
                             prices: modelTransformer.transform(prices, Price)}]);
        };

        var errorGetPrices = function () {
            $scope.publish(events.message._GET_PRICE_BY_CAMPAIGN_ID_FAILED_);
            $scope.publish(events.message._ADD_ERROR_MESSAGE_,
                           ['Unable to get prices for campaign by id from server',
                            'alert.warning']);
        };

        $scope.subscribe(events.message._GET_PRICE_BY_CAMPAIGN_ID_, getPrices);

        /***** CANCEL *****/
        var cancelCampaign = function(campaign) {
            $scope.traceDebug('receive _CAMPAIGN_RUN_CANCEL_  msg ' +
                              JSON.stringify(campaign) );
            return api.campaign.cancel(campaign).then(successHdlCancel, errorHdlCancel);
        };
        // };
        var successHdlCancel = function(response) {
            $scope.traceDebug('CAMPAIGN_RUN_CANCEL SUCCESS' +
                              JSON.stringify(response.data));
            if (response.data) {
                $scope.publish(events.message._CAMPAIGN_RUN_CANCEL_COMPLETE_);
            } else {
                errorHdlCancel();
            }
        };

        //                        [modelTransformer.transform(response.data,
        //                                                    Campaign)]);

        var errorHdlCancel = function() {
            $scope.traceDebug('CAMPAIGN_RUN_CANCEL FAILURE');
            $scope.publish(events.message._CAMPAIGN_RUN_CANCEL_FAILED_);
        };

        $scope.subscribe(events.message._CAMPAIGN_RUN_CANCEL_, cancelCampaign);
        /******************* CREATE ********************/

        var create = function(campaign) {
            $scope.traceDebug('receive CAMPAIGN_CREATE msg ' +
                              JSON.stringify(campaign) );
            return api.campaign.create(campaign).then(successHdlCreate, errorHdlCreate);
        };

        var successHdlCreate = function(response) {
            $scope.traceDebug('CAMPAIGN_CREATE SUCCESS' +
                              JSON.stringify(response.data));
            if (response.data) {
                $scope.publish(events.message._CAMPAIGN_CREATE_COMPLETE_,
                               [modelTransformer.transform(response.data,
                                                           Campaign)]);
            }
            else {
                errorHdlCreate(response);
            }
        };

        var errorHdlCreate = function (response) {
            $scope.traceDebug('CAMPAIGN_CREATE FAILURE ' + JSON.stringify(response.data));
            $scope.publish(events.message._CAMPAIGN_CREATE_FAILED_,
                           [response.data]);
            // $scope.publish(events.message._ADD_ERROR_MESSAGE_,
            //                ['Unable to create campaign',
            //                 'alert.warning']);
        };

        $scope.subscribe(events.message._CAMPAIGN_CREATE_, create);

        /******************* DESTROY ********************/
        var destroy = function(campaignId) {
            $scope.traceDebug("ready to remove " + campaignId);
            return api.campaign.delete(campaignId).then(successHdlDestroy,
                                                        errorHdlDestroy);
        };

        var successHdlDestroy = function(response) {
            $scope.traceDebug("SUCCES destroy campaign " + JSON.stringify(response));
            if (response.data) {
                var id = response.data.campaignId;
                $scope.publish(events.message._DESTROY_CAMPAIGN_COMPLETE_, [id]);
            } else {
                errorHdlDestroy();
            }
        };

        var errorHdlDestroy = function () {
            $scope.traceDebug("ERROR destroy campaign " + JSON.stringify(response));
            $scope.publish(events.message._DESTROY_CAMPAIGN_FAILED_);
        };

        $scope.subscribe(events.message._DESTROY_CAMPAIGN_, destroy);

        /******************* UPDATE ********************/

        var update = function(campaign) {
            return api.campaign.update(campaign).then(successHdlUpdate,
                                                      errorHdlUpdate);
        };

        var successHdlUpdate = function(response) {
            if (response.data) {
                $scope.publish(events.message._CAMPAIGN_UPDATE_COMPLETE_,
                               [modelTransformer.transform(response.data,
                                                           Campaign)]);
            }
            else {
                errorHdlUpdate();
            }
        };

        var errorHdlUpdate = function () {
            $scope.publish(events.message._CAMPAIGN_UPDATE_FAILED_);
            $scope.publish(events.message._ADD_ERROR_MESSAGE_,
                           ['Unable to update campaign from server',
                            'alert.warning']);
        };

        $scope.subscribe(events.message._CAMPAIGN_UPDATE_, update);


        var init = function () {
            $scope.traceDebug('into init campaignDataSrvc subscribe ' +
                              events.message._CAMPAIGN_CREATE_);
            campaign = [];
        };

        var campaignDataService = {
            init: init,
            all: all,
            getById: getById,
            create: create,
            update: update,
            destroy: destroy
        };
        return campaignDataService;
    }
})();
