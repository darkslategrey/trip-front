(function () {
    'use strict';

    angular.module('app.dataservices').factory('proxyDataService',
                                               ProxyDataService);

    /** @ngInject */
    function ProxyDataService(events, modelTransformer, api,
                              Proxy, $rootScope, $controller) {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', {$scope: $scope});

        $scope.name = 'ProxyDataService';

        var proxy = [];

        /******************* CREATE ONE  ********************/
        var create = function(proxy) {
            $scope.traceDebug('create proxy ' + JSON.stringify(proxy));
            return api.proxy.create(proxy).then(successCreateOne, errorCreateOne);
        };

        var successCreateOne = function(response) {
            $scope.traceDebug('create proxy success ' + JSON.stringify(response));

            if(!_.has(response, 'data')) {
                errorCreateOne(response);
            } else {
                $scope.publish(events.message._PROXY_CREATE_COMPLETE_, [response.id]);
            }
        };

        var errorCreateOne = function(response) {
            $scope.traceDebug('create proxy failure ' + JSON.stringify(response));
            $scope.publish(events.message._PROXY_CREATE_FAILED_);
        };

        /******************* UPDATE ONE  ********************/
        var update = function(proxy) {
            $scope.traceDebug('update proxy ' + JSON.stringify(proxy));
            return api.proxy.update(proxy).then(successUpdateOne, errorUpdateOne);
        };

        var successUpdateOne = function(response) {
            $scope.traceDebug('update proxy success ' + JSON.stringify(response));

            if(!_.has(response, 'data')) {
                errorUpdateOne(response);
            } else {
                $scope.publish(events.message._PROXY_UPDATE_COMPLETE_, [response]);
            }
        };

        var errorUpdateOne = function(response) {
            $scope.traceDebug('update proxy failure ' + JSON.stringify(response));
            $scope.publish(events.message._PROXY_UPDATE_FAILED_);
        };

        /******************* DELETE ONE  ********************/
        var remove = function(proxyId) {
            $scope.traceDebug('remove proxy ' + JSON.stringify(proxyId));
            return api.proxy.delete(proxyId).then(successRemoveOne, errorRemoveOne);
        };

        var successRemoveOne = function(response) {
            $scope.traceDebug('remove proxy success ' + JSON.stringify(response));

            if(!_.has(response, 'data')) {
                errorRemoveOne(response);
            } else {
                $scope.publish(events.message._PROXY_REMOVE_COMPLETE_, [response.id]);
            }
        };

        var errorRemoveOne = function(response) {
            $scope.traceDebug('remove proxy failure ' + JSON.stringify(response));
            $scope.publish(events.message._PROXY_REMOVE_FAILED_);
        };

        /******************* ALL ********************/

        var all = function() {
            $scope.traceDebug('get all proxies');
            return api.proxy.all().then(successAll, errorAll);
        };

        var successAll = function(response) {
            $scope.traceDebug('allProxySuccessHandler success ');
            // $scope.traceDebug('resposne ' + JSON.stringify(response));
            var result = [];
            angular.forEach(response, function (proxy) {
                result.push(modelTransformer.transform(proxy, Proxy));
            });
            $scope.publish(events.message._ALL_PROXY_COMPLETE_, [result]);
        };

        var errorAll = function () {
            $scope.publish(events.message._ALL_PROXY_FAILED_);
        };

        $scope.subscribe(events.message._ALL_PROXY_, all);

        /******************* UPDATE ********************/

        var updateAll = function(proxies) {
            $scope.traceDebug('update proxies');
            return api.proxy.updateAll(proxies).then(successUpdate, errorUpdate);
        };

        var successUpdate = function(response) {
            $scope.traceDebug('update proxies success: ' + JSON.stringify(response));
            $scope.publish(events.message._PROXIES_UPDATE_COMPLETE_, [response.data]);
        };

        var errorUpdate  = function(response) {
            // $scope.traceDebug('update proxies error: ' + JSON.stringify(response));
            $scope.publish(events.message._PROXIES_UPDATE_FAILED_, [response]);
        };

        $scope.subscribe(events.message._PROXIES_UPDATE_, updateAll);
        $scope.subscribe(events.message._PROXY_CREATE_, create);
        $scope.subscribe(events.message._PROXY_UPDATE_, update);
        $scope.subscribe(events.message._PROXY_REMOVE_, remove);

        var init = function () {
            proxy = [];
        };

        var proxyDataService = {
            init: init,
            all: all,
            updateAll: updateAll,
            create: create,
            remove: remove,
            update: update

        };
        return proxyDataService;
    }
}());
