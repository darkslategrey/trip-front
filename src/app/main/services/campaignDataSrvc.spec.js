//   TODO: complete specs
describe('campaignDataService', function() {
    var rootScope = null;
    var service = null;
    var api = null;
    var pubsub = null;
    var constants = null;
    var httpBackend = null;
    var q = null;

    beforeEach(module('app'));
    beforeEach(module('app.dataservices'));
    beforeEach(module('app.test'));
    beforeEach(module('generatorGulpAngular'));
    // beforeEach(module('fuse'));

    beforeEach(inject(function ($rootScope, $q, campaignDataService,
                                _api_, messaging, events) {
        rootScope = $rootScope;
        service = campaignDataService;
        api = _api_;
        pubsub = messaging;
        q = $q;
        expect(pubsub.publish('hello', '')).toEqual('hello');
        constants = events;
    }));

    it('should get the full list of campaigns', function () {
        spyOn(pubsub, 'publish').and.callThrough();

        service.all().then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish)
                .toHaveBeenCalledWith(constants.message._ALL_CAMPAIGN_COMPLETE_,
                                      jasmine.any(Array));
        });
        var res = {data: [{id: 1, name: 'c1'}, {id: 2, name: 'c2'} ]};

        api.campaign.deferred.resolve(res);

        rootScope.$digest();
    });

    it('should get a campaign by id',          function() {
        spyOn(pubsub, 'publish').and.callThrough();

        service.getById(2).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._CAMPAIGN_GET_BY_ID_COMPLETE_,
                                                        jasmine.any(Array));
        });

        var res = {data: [{id: 1, name: 'c1'}, {id: 2, name: 'c2'} ]};

        api.campaign.deferred.resolve(res);

        rootScope.$digest();
    });

    it('should create a campaign',            function() {
        spyOn(api.campaign, 'create').and.callThrough();

        var campaign = {
            fromSlot: '02/01/2017', toSlot: '01/02/2017', proxies: [ '0' ],
            hotels: [ '0' ], markets: [ '0' ], timestamp: moment().format(), id: 1, name: 'c1'
        };

        service.create(campaign).then(function(){
            expect(api.campaign.create).toHaveBeenCalled();
            expect(api.campaign.create).toHaveBeenCalledWith(campaign);
        });

        var result = {
            data: [ campaign ]
        };

        api.campaign.deferred.resolve(result);

        rootScope.$digest();
    });

    it('should create a campaign and run it', function() {});
    it('should update a campaign', function() {});
    it('should delete a campaign', function() {});


    xit('should publish the message _GET_CAMPAIGN_COMPLETE_ after all campaign are received', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var result = {
            data: [
                {"Name": "Pot ( 2 Gal/7.6 L) - Extract", "TUNVolume": 256.0000000, "TUNWeight": 40.0000000, "TUNSpecificHeat": 0.1200000,"TUNDeadSpace": 0.0000000,"CalcBoilTime": 1,"BoilSize": 217.6000000,"BoilTime": 60.0000000,"EvapRate": 9.0000000,"BoilOff": 17.9200000,"TrubChillerLoss": 32.0000000,"CoolingPercentage": 4.0000000,"TopUpKettle": 0.0000000,"BatchSize": 640.0000000,"FermenterLoss": 51.2000000,"TopUpWater": 480.0000000,"Efficiency": 72.0000000,"HopUtilization": 100.0000000,"Notes": "Simple brew pot with 2 gallon capacity for extract brewing.  May also be used for partial mash with temperature or infusion mashing."},
                {"Name": "Pot ( 3 Gal/11.4 L) - Extract", "TUNVolume": 384.0000000, "TUNWeight": 48.0000000, "TUNSpecificHeat": 0.1200000, "TUNDeadSpace": 0.0000000, "CalcBoilTime": 1, "BoilSize": 360.9600000, "BoilTime": 60.0000000, "EvapRate": 9.0000000, "BoilOff": 28.1600000, "TrubChillerLoss": 64.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 640.0000000, "FermenterLoss": 51.2000000, "TopUpWater": 384.0000000, "Efficiency": 72.0000000, "HopUtilization": 100.0000000, "Notes": "Simple Brew Pot with a 3 Gallon Capacity - leaving a workable boil volume of around 2.5 gal.  Used for extract or partial mash brewing."},
                {"Name": "Pot ( 4 Gal/15.1 L) - Extract", "TUNVolume": 512.0000000, "TUNWeight": 64.0000000, "TUNSpecificHeat": 0.1200000, "TUNDeadSpace": 0.0000000, "CalcBoilTime": 1, "BoilSize": 469.7600000, "BoilTime": 60.0000000, "EvapRate": 9.0000000, "BoilOff": 37.1200000, "TrubChillerLoss": 64.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 640.0000000, "FermenterLoss": 51.2000000, "TopUpWater": 288.0000000, "Efficiency": 72.0000000, "HopUtilization": 100.0000000, "Notes": "Simple Brew Pot with a 4 Gallon Capacity - leaving a workable boil volume of around 3.25 gal.  Used for extract or partial mash brewing."},
                {"Name": "Pot and Cooler ( 5 Gal/19 L) - All Grain", "TUNVolume": 640.0000000, "TUNWeight": 64.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 32.0000000, "CalcBoilTime": 1, "BoilSize": 834.5600000, "BoilTime": 60.0000000, "EvapRate": 9.0000000, "BoilOff": 69.1200000, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 640.0000000, "FermenterLoss": 51.2000000, "TopUpWater": 0.0000000, "Efficiency": 72.0000000, "HopUtilization": 100.0000000, "Notes": "Popular all grain setup.  5 Gallon cooler as mash tun with false bottom, and 7-9 gallon brewpot capable of boiling at least 6 gallons of wort without spilling over.  Primarily used for single infusion mashes."}
            ]
        };

        service.getCampaign().then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._GET_CAMPAIGN_COMPLETE_, [result.data]);
        });

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _GET_CAMPAIGN_FAILED_ when query request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        service.getCampaign().then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._GET_CAMPAIGN_FAILED_);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should publish the message _ADD_ERROR_MESSAGE_ when query request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        service.getCampaign().then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith('_ADD_ERROR_MESSAGE_', [ 'Unable to get campaign from server', 'alert.warning' ]);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should request queryById when getCampaignById is called', function () {
        spyOn(api, 'queryById').andCallThrough();

        service.getCampaignById('123').then(function(){
            expect(api.queryById).toHaveBeenCalled();
        });

        var result = {
            data: [
                {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."}
            ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should send database and collection name and id when requesting queryById', function () {
        spyOn(api, 'queryById').andCallThrough();

        service.getCampaignById('123').then(function(){
            expect(api.queryById).toHaveBeenCalled();
            expect(api.queryById).toHaveBeenCalledWith('brew_everywhere', 'campaign', '123', []);
        });

        var result = {
            data: [
                {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."}
            ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _GET_CAMPAIGN_BY_ID_COMPLETE_ when queryById succeeds', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var result = {
            data: [
                {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."}
            ]
        };

        service.getCampaignById('123').then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._GET_CAMPAIGN_BY_ID_COMPLETE_, [result.data]);
        });

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _GET_CAMPAIGN_BY_ID_FAILED_ when queryByID request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        service.getCampaignById('123').then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._GET_CAMPAIGN_BY_ID_FAILED_);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should publish the message _ADD_ERROR_MESSAGE_ when queryById request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        service.getCampaignById('123').then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith('_ADD_ERROR_MESSAGE_', [ 'Unable to get campaign by id from server', 'alert.warning' ]);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should request create when createCampaign is called', function () {
        spyOn(api, 'create').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.createCampaign(campaign).then(function(){
            expect(api.create).toHaveBeenCalled();
        });

        var result = {
            data: [ campaign ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should send database and collection name and object when requesting create', function () {
        spyOn(api, 'create').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.createCampaign(campaign).then(function(){
            expect(api.create).toHaveBeenCalled();
            expect(api.create).toHaveBeenCalledWith('brew_everywhere', 'campaign', campaign);
        });

        var result = {
            data: [ campaign ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _CREATE_CAMPAIGN_COMPLETE_ when create succeeds', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.createCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._CREATE_CAMPAIGN_COMPLETE_, [result.data]);
        });

        var result = {
            data: [ campaign ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _CREATE_CAMPAIGN_FAILED_ when create request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.createCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._CREATE_CAMPAIGN_FAILED_);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should publish the message _ADD_ERROR_MESSAGE_ when create request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.createCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith('_ADD_ERROR_MESSAGE_', [ 'Unable to create campaign', 'alert.warning' ]);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should request update when updateCampaign is called', function () {
        spyOn(api, 'update').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.updateCampaign(campaign).then(function(){
            expect(api.update).toHaveBeenCalled();
        });

        var result = {
            data: [ campaign ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should send database and collection name and object when requesting update', function () {
        spyOn(api, 'update').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.updateCampaign(campaign).then(function(){
            expect(api.update).toHaveBeenCalled();
            expect(api.update).toHaveBeenCalledWith('brew_everywhere', 'campaign', campaign);
        });

        var result = {
            data: [ campaign ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _CAMPPAIGN_UPDATE_COMPLETE_ when update succeeds', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.updateCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._CAMPPAIGN_UPDATE_COMPLETE_, [result.data]);
        });

        var result = {
            data: [campaign]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _CAMPPAIGN_UPDATE_FAILED_ when update request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.updateCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._CAMPPAIGN_UPDATE_FAILED_);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should publish the message _ADD_ERROR_MESSAGE_ when update request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.updateCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith('_ADD_ERROR_MESSAGE_', [ 'Unable to update campaign', 'alert.warning' ]);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should request delete when deleteCampaign is called', function () {
        spyOn(api, 'delete').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.deleteCampaign(campaign).then(function(){
            expect(api.delete).toHaveBeenCalled();
        });

        var result = {
            data: [ campaign ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should send database and collection name and object when requesting delete', function () {
        spyOn(api, 'delete').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.deleteCampaign(campaign).then(function(){
            expect(api.delete).toHaveBeenCalled();
            expect(api.delete).toHaveBeenCalledWith('brew_everywhere', 'campaign', campaign);
        });

        var result = {
            data: [ campaign ]
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _DELETE_CAMPAIGN_COMPLETE_ when delete succeeds', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.deleteCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._DELETE_CAMPAIGN_COMPLETE_);
        });

        var result = {
            status: 200
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _DELETE_CAMPAIGN_FAILED_ if status code is not 200', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.deleteCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._DELETE_CAMPAIGN_FAILED_);
        });

        var result = {
            status: 400
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _ADD_ERROR_MESSAGE_ if status code is not 200', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.deleteCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith('_ADD_ERROR_MESSAGE_', [ 'Unable to delete campaign', 'alert.warning' ]);
        });

        var result = {
            status: 400
        };

        api.deferred.resolve(result);

        rootScope.$digest();
    });

    xit('should publish the message _DELETE_CAMPAIGN_FAILED_ when delete request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.deleteCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith(constants.message._DELETE_CAMPAIGN_FAILED_);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

    xit('should publish the message _ADD_ERROR_MESSAGE_ when delete request fails', function () {
        spyOn(pubsub, 'publish').andCallThrough();

        var campaign = {"Name": "Pot (18.5 Gal/70 L) and Cooler (9.5 Gal/40 L)  - All Grain", "TUNVolume": 1280.0000000, "TUNWeight": 144.0000000, "TUNSpecificHeat": 0.3000000, "TUNDeadSpace": 102.4000000, "CalcBoilTime": 1, "BoilSize": 1253.1200000, "BoilTime": 90.0000000, "EvapRate": 9.0000000, "BoilOff": 229.5466667, "TrubChillerLoss": 96.0000000, "CoolingPercentage": 4.0000000, "TopUpKettle": 0.0000000, "BatchSize": 778.2400000, "FermenterLoss": 57.6000000, "TopUpWater": 0.0000000, "Efficiency": 68.0000000, "HopUtilization": 100.0000000, "Notes": "Based on a 18.5 Gal/70 L pot with a diameter of 18 inches/45 cm and a 9.5 Gal/40 L cooler. The above assumes loose pellet hops and only clear, chilled wort transferred from the kettle using no trub management techniques. Experienced brewers should adjust &#39;Loss to Trub and Chiller&#39; and &#39;Brewhouse Efficiency&#39; accordingly to suit their trub management techniques."};

        service.deleteCampaign(campaign).then(function(){
            expect(pubsub.publish).toHaveBeenCalled();
            expect(pubsub.publish).toHaveBeenCalledWith('_ADD_ERROR_MESSAGE_', [ 'Unable to delete campaign', 'alert.warning' ]);
        });

        api.deferred.reject();

        rootScope.$digest();
    });

});
