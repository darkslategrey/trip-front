(function () {

    'use strict';

    angular.module('app.dataservices').factory('veilleDataService',
                                               VeilleDataService);

    /** @ngInject */
    function VeilleDataService(events, modelTransformer, api,
                               Veille,
                               $log, $rootScope, $controller, Restangular) {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', {$scope: $scope});

        $scope.name = 'VeilleDataService';

        var veille = [];

        /*** ALL ****/
        var all = function() {
            return api.veille.all().then(successAll, errorAll);
        };
        var successAll = function(response) {
            var result = [];
            angular.forEach(response, function (veille) {
                result.push(modelTransformer.transform(veille, Veille));
            });
            $scope.publish(events.message._ALL_VEILLE_COMPLETE_, [result]);
        };
        var errorAll = function() {
            $scope.publish(events.message._ALL_VEILLE_FAILED_);
        };
        $scope.subscribe(events.message._ALL_VEILLE_, all);

        /**** CREATE *****/
        var create = function(veille) {
            $scope.traceDebug('receive VEILLE_CREATE msg ' +
                              JSON.stringify(veille) );
            return api.veille.create(veille).then(successCreate, errorCreate);
        };
        var successCreate = function(response) {
            $scope.traceDebug('VEILLE_CREATE SUCCESS' +
                              JSON.stringify(response.data));
            if (response.data) {
                $scope.publish(events.message._VEILLE_CREATE_COMPLETE_,
                               [modelTransformer.transform(response.data,
                                                           Veille)]);
            } else { errorCreate(); }
        };
        var errorCreate = function () {
            $scope.traceDebug('VEILLE_CREATE FAILURE');
            $scope.publish(events.message._VEILLE_CREATE_FAILED_);
        };
        $scope.subscribe(events.message._VEILLE_CREATE_, create);

        /** UPDATE ***/
        var update = function(veille) {
            $scope.traceDebug('receive VEILLE_UPDATE msg ' +
                              JSON.stringify(veille) );
            return api.veille.update(veille).then(successUpdate, errorUpdate);
        };
        var successUpdate = function(response) {
            $scope.traceDebug('VEILLE_UPDATE SUCCESS' +
                              JSON.stringify(response.data));
            if (response.data) {
                $scope.publish(events.message._VEILLE_UPDATE_COMPLETE_);
            } else { errorUpdate(response); }
        };
        var errorUpdate = function (response) {
            $scope.traceDebug('VEILLE_UPDATE FAILURE');
            $scope.publish(events.message._VEILLE_UPDATE_FAILED_);
        };
        $scope.subscribe(events.message._VEILLE_UPDATE_, update);

        /** REMOVE ***/
        var remove = function(veilleId) {
            $scope.traceDebug('receive VEILLE_REMOVE msg ' + veilleId);
            return api.veille.delete(veilleId).then(successRemove, errorRemove);
        };
        var successRemove = function(response) {
            $scope.traceDebug('VEILLE_REMOVE SUCCESS' +
                              JSON.stringify(response.data));
            if (response.data) {
                $scope.publish(events.message._VEILLE_REMOVE_COMPLETE_,
                               [response.data.veilleId]);
            } else { errorRemove(); }
        };
        var errorRemove = function () {
            $scope.traceDebug('VEILLE_REMOVE FAILURE');
            $scope.publish(events.message._VEILLE_REMOVE_FAILED_);
        };
        $scope.subscribe(events.message._VEILLE_REMOVE_, remove);

        var init = function () {
            veille = [];
        };
        var veilleDataService = {
            init: init,
            all: all,
            create: create,
            update: update,
            remove: remove
        };
        return veilleDataService;
    }
})();
