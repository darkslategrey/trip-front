(function () {

    'use strict';

    angular.module('app.dataservices').factory('priceDataService',
                                               PriceDataService);

    /** @ngInject */
    function PriceDataService(events, modelTransformer, api, Price,
                              $log, $rootScope, $controller, $q) {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', {$scope: $scope});

        $scope.name = 'PriceDataService';

        var price = [];

        /******************* GET BY Campaign ID ********************/

        var getByCampaignId = function(id) {
            var promise = null;
            if(_.has(price, id)) {
                $scope.traceDebug('get prices from cache');
                var deferred = $q.defer();
                deferred.resolve(price[id]);
                promise = deferred.promise;
            } else {
                $scope.traceDebug('get prices from api');
                promise = api.campaign.getPricesByCampaignId(id);
            }
            return promise.then(successHdlId, errorHdlId);
        };

        var successHdlId = function(response) {
            if (response.data) {
                var transformedPrices = modelTransformer.transform(response.data,
                                                                   Price);
                price[response.data.campaignId] = response;
                $scope.publish(events.message._PRICE_GET_BY_CAMPAIGN_ID_COMPLETE_,
                               [transformedPrices]);
            }
            else {
                errorHdlId();
            }
        };

        var errorHdlId = function () {
            $scope.publish(events.message._PRICE_GET_BY_CAMPAIGN_ID_FAILED_);
            $scope.publish(events.message._ADD_ERROR_MESSAGE_,
                           ['Unable to get price by id from server',
                            'alert.warning']);
        };

        // $scope.subscribe(events.message._GET_PRICE_BY_CAMPAIGN_ID_, getByCampaignId);

        var init = function () {
            price = {};
        };

        var priceDataService = {
            init: init,
            getByCampaignId: getByCampaignId
        };
        return priceDataService;
    }
})();
