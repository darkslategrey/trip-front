describe('progress bar', function () {


    beforeEach(function() {
        module('app.components');
    });

    var $rootScope, messaging, $componentCtrl, ctrl,
        events, $scope, $interval, $log, $httpBackend;

    beforeEach(inject(function ($injector) {
        $rootScope     = $injector.get('$rootScope');
        $componentCtrl = $injector.get('$componentController');
        messaging      = $injector.get('messaging');
        events         = $injector.get('events');
        $interval      = $injector.get('$interval');
        $scope         = $rootScope.$new();
        $log           = $injector.get('$log');


        $httpBackend = $injector.get('$httpBackend');
        // backend definition common for all tests
        // authRequestHandler = $httpBackend.when('GET', '/api/checkprogress/123')
        //     .respond({value: 40});

        var bindings = { };
        var locals   = { }; // messaging: messaging
        ctrl = $componentCtrl('runProgressbar', null, bindings);
    }));

    afterEach(function() {
        console.log($log.debug.logs);
        console.log($log.log.logs);
        console.log($log.error.logs);
        // $httpBackend.verifyNoOutstandingExpectation();
        // $httpBackend.verifyNoOutstandingRequest();
    });

    it('can be instanciated', function() {
        expect(ctrl).toBeDefined();
        expect(messaging).toBeDefined();
    });

    it('should call the handler', function() {
        // $httpBackend.expectGET('/api/checkprogress/123');
        ctrl.campaignCreateHandler();
        // $interval.flush();
        // $httpBackend.flush();
        messaging.publish(events.message._CHECK_PROGRESS_DATA_, [50]);
        expect(ctrl.determinateValue).toEqual(50);
    });

});
