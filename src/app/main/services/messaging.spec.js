describe('messaging', function () {
    var $rootScope, messaging;

    beforeEach(function() {
        module('app');
    });

    it('should store the subscriber in the cache', function() {
        var callbackHandler = function (value) {
            expect(value).toBeTruthy();
        };

        messaging.subscribe('TEST', callbackHandler);

        messaging.publish('TEST', ['value']);

    });

    beforeEach(inject(function ($injector) {
        messaging  = $injector.get('messaging');
        $rootScope = $injector.get('$rootScope');
    }));

    it('should pass through the values published', function() {
        expect(true).toBe(true);
        var  actual = 'TEST';
        var callbackHandler = function (value) {
            expect(value).toBe(actual);
        };

        messaging.subscribe('TEST', callbackHandler);

        messaging.publish('TEST', [actual]);

    });

    it('should remove the callback handler when unsubscribed', function() {
        var  actual = 'TEST';
        var callbackHandler = function (value) {
            expect(value).toBe(actual);
        };

        var handle = messaging.subscribe('TEST', callbackHandler);

        messaging.publish('TEST', [actual]);

        messaging.unsubscribe(handle);

        actual = 'NEW VALUE';

        messaging.publish('TEST', [actual]);
    });
});
