(function() {

    'use strict';

    angular.module('app').constant('events', {
        message: {
            _SERVER_REQUEST_STARTED_: '_SERVER_REQUEST_STARTED_',
            _SERVER_REQUEST_ENDED_: '_SERVER_REQUEST_ENDED_',

            _CHECK_PROGRESS_CREATE_: '_CHECK_PROGRESS_CREATE_',
            _CHECK_PROGRESS_START_: '_CHECK_PROGRESS_START_',
            _CHECK_PROGRESS_END_: '_CHECK_PROGRESS_END_',
            _CHECK_PROGRESS_DATA_: '_CHECK_PROGRESS_DATA_',
            _CHECK_PROGRESS_ERROR_: '_CHECK_PROGRESS_ERROR_',

            _LOG_TRACE_: '_LOG_TRACE_',
            _LOG_DEBUG_: '_LOG_DEBUG_',
            _LOG_INFO_: '_LOG_INFO_',
            _LOG_WARNING_: '_LOG_WARNING_',
            _LOG_ERROR_: '_LOG_ERROR_',
            _LOG_FATAL_: '_LOG_FATAL_',

            _ALL_CAMPAIGN_: '_ALL_CAMPAIGN_',
            _ALL_CAMPAIGN_COMPLETE_: '_ALL_CAMPAIGN_COMPLETE_',
            _ALL_CAMPAIGN_FAILED_: '_ALL_CAMPAIGN_FAILED_',

            _ALL_VEILLE_: '_ALL_VEILLE_',
            _ALL_VEILLE_COMPLETE_: '_ALL_VEILLE_COMPLETE_',
            _ALL_VEILLE_FAILED_: '_ALL_VEILLE_FAILED_',

            _VEILLE_CREATE_: '_VEILLE_CREATE_',
            _VEILLE_CREATE_COMPLETE_: '_VEILLE_CREATE_COMPLETE_',
            _VEILLE_CREATE_FAILED_: '_VEILLE_CREATE_FAILED_',

            _VEILLE_UPDATE_: '_VEILLE_UPDATE_',
            _VEILLE_UPDATE_COMPLETE_: '_VEILLE_UPDATE_COMPLETE_',
            _VEILLE_UPDATE_FAILED_: '_VEILLE_UPDATE_FAILED_',

            _VEILLE_REMOVE_: '_VEILLE_REMOVE_',
            _VEILLE_REMOVE_COMPLETE_: '_VEILLE_REMOVE_COMPLETE_',
            _VEILLE_REMOVE_FAILED_: '_VEILLE_REMOVE_FAILED_',

            _PRICE_CREATE_: '_PRICE_CREATE_',
            _PRICE_CREATE_COMPLETE_: '_PRICE_CREATE_COMPLETE_',
            _PRICE_CREATE_FAILED_: '_PRICE_CREATE_FAILED_',

            _PROXIES_UPDATE_: '_PROXIES_UPDATE_',
            _PROXIES_UPDATE_COMPLETE_: '_PROXIES_UPDATE_COMPLETE_',
            _PROXIES_UPDATE_FAILED_: '_PROXIES_UPDATE_FAILED_',

            _PROXY_UPDATE_: '_PROXY_UPDATE_',
            _PROXY_UPDATE_COMPLETE_: '_PROXY_UPDATE_COMPLETE_',
            _PROXY_UPDATE_FAILED_: '_PROXY_UPDATE_FAILED_',

            _PROXY_CREATE_: '_PROXY_CREATE_',
            _PROXY_CREATE_COMPLETE_: '_PROXY_CREATE_COMPLETE_',
            _PROXY_CREATE_FAILED_: '_PROXY_CREATE_FAILED_',

            _PROXY_REMOVE_: '_PROXY_REMOVE_',
            _PROXY_REMOVE_COMPLETE_: '_PROXY_REMOVE_COMPLETE_',
            _PROXY_REMOVE_FAILED_: '_PROXY_REMOVE_FAILED_',

            _GET_PRICE_BY_CAMPAIGN_ID_: '_GET_PRICE_BY_CAMPAIGN_ID_',
            _GET_PRICE_BY_CAMPAIGN_ID_COMPLETE_: '_GET_PRICE_BY_CAMPAIGN_ID_COMPLETE_',
            _GET_PRICE_BY_CAMPAIGN_ID_FAILED_: '_GET_PRICE_BY_CAMPAIGN_ID_FAILED_',



            _CAMPAIGN_CREATE_: '_CAMPAIGN_CREATE_',
            _CAMPAIGN_CREATE_ID_AVAILABLE_: '_CAMPAIGN_CREATE_ID_AVAILABLE_',
            _CAMPAIGN_CREATE_COMPLETE_: '_CAMPAIGN_CREATE_COMPLETE_',
            _CAMPAIGN_CREATE_FAILED_: '_CAMPAIGN_CREATE_FAILED_',

            _GET_CAMPAIGN_BY_ID_: '_GET_CAMPAIGN_BY_ID_',
            _GET_CAMPAIGN_BY_ID_COMPLETE_: '_GET_CAMPAIGN_BY_ID_COMPLETE_',
            _GET_CAMPAIGN_BY_ID_FAILED_: '_GET_CAMPAIGN_BY_ID_FAILED_',

            _CAMPAIGN_UPDATE_: '_CAMPAIGN_UPDATE_',
            _CAMPAIGN_UPDATE_COMPLETE_: '_CAMPAIGN_UPDATE_COMPLETE_',
            _CAMPAIGN_UPDATE_FAILED_: '_CAMPAIGN_UPDATE_FAILED_',

            _CAMPAIGN_RUN_: '_CAMPAIGN_RUN_',
            _CAMPAIGN_RUN_COMPLETE_: '_CAMPAIGN_RUN_COMPLETE_',
            _CAMPAIGN_RUN_FAILED_: '_CAMPAIGN_RUN_FAILED_',
            _CAMPAIGN_RUN_CANCEL_: '_CAMPAIGN_RUN_CANCEL_',

            _DESTROY_CAMPAIGN_: '_DESTROY_CAMPAIGN_',
            _DESTROY_CAMPAIGN_COMPLETE_: '_DESTROY_CAMPAIGN_COMPLETE_',
            _DESTROY_CAMPAIGN_FAILED_: '_DESTROY_CAMPAIGN_FAILED_',


            _ADD_ERROR_MESSAGE_: '_ADD_ERROR_MESSAGE_',
            _CLEAR_ERROR_MESSAGES_: '_CLEAR_ERROR_MESSAGES_',
            _ERROR_MESSAGES_UPDATED_: '_ERROR_MESSAGES_UPDATED_',
            _ADD_USER_MESSAGE_: '_ADD_USER_MESSAGE_',
            _CLEAR_USER_MESSAGES_: '_CLEAR_USER_MESSAGES_',

            _CONSULT_COMPLETE_: '_CONSULT_COMPLETE_'

        }
    });
    // $controller
    angular.module('app').factory('messaging', function () {
        //#region Internal Methods
        var cache = {};

        var subscribe = function (topic, callback) {
            if (!cache[topic]) {
                cache[topic] = [];
            }
            cache[topic].push(callback);
            return [topic, callback];
        };

        var publish = function (topic, args) {
            if (cache[topic]) {
                angular.forEach(cache[topic], function (callback) {
                    callback.apply(null, args || []);
                });
            }
        };

        var unsubscribe = function (handle) {
            var t = handle[0];
            if (cache[t]) {
                for(var x = 0; x < cache[t].length; x++)
                {
                    if (cache[t][x] === handle[1]) {
                        cache[t].splice(x, 1);
                    }
                }
            }
        };

        var service = {
            publish: publish,
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };

        return service;
    });
})();

