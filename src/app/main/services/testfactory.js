(function () {

    'use strict';
    angular.module('app').factory('testFactory', function () {

        var myTestFunc = function() {
            console.log('myTestFunc call!');
        };

        var services = {
            publish: myTestFunc
        };

        return services;

    });

}());
