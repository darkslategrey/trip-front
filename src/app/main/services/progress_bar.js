(function() {
    'use strict';

    angular
        .module('app.components')
        .component('runProgressbar',  {
            bindings: {
                show: '<'
            },
            template:'',
            // template: '<div ng-show="$ctrl.show" class="container progressLineardemoBasicUsage" flex>'+
            //     '<md-progress-linear md-mode="query"></md-progress-linear>'+
            //     '<div class="bottom-block">'+
            //     '<span><ul>' +
            //     '<li ng-repeat="msg in $ctrl.msgs">{{msg}}</li></ul></span></div></div>',
//                '<span ng-bind-html="$ctrl.progressMsg"></span></div></div>',
            controller: ProgressBarCtrl
        });

    /** @ngInject */
    function ProgressBarCtrl($controller, $scope, $timeout, events,
                             $interval, checkprogress, $log, $sce) {
        $controller('BaseCtrl', {$scope: $scope});
        var self = this;

        $scope.name = 'progressbarctrl';

        var progress = 1;

        self.onCampaignCreate = function() {
            self.msgs = [];
            self.show = true;
            // self.progressMsg = $sce.trustAsHtml("Creation en cours...");
            self.msgs.push($sce.trustAsHtml("Creation en cours..."));
        };
        self.onCampaignCreateComplete = function() {
            self.msgs[0] = $sce.trustAsHtml(self.msgs[0] + "Ok");
            // self.progressMsg = $sce.trustAsHtml(self.progressMsg + "Ok</br>");
        };

        self.onCheckProgressStart = function() {
            var msg = "Collecte en cours, merci de patienter...";
            // blockUI.start(msg);
            self.msgs.push($sce.trustAsHtml(msg));
            // self.progressMsg = $sce.trustAsHtml(self.progressMsg + msg);
        };
        self.onCheckProgressData = function(progress) {
            var text_value = _.split(_.last(self.msgs), '...');
            var text       = text_value[0];
            var value      = progress;
            var newtext    = text + '...' + value + '%';
            // blockUI.message(newtext);
            $scope.traceDebug('get progress data new text <' + newtext + '>');
            self.msgs[self.msgs.length - 1] = newtext;
        };
        self.onCheckProgressEnd = function(campaignId) {
            $scope.traceDebug('check progress end <' + campaignId + '>');
            var text_value = _.split(_.last(self.msgs), '...');
            self.msgs[-1] = text + '...OK';
            $scope.publish(events.message._CAMPAIGN_RUN_COMPLETE_, [campaignId]);
        };
        self.onGetPricesByCampaignId = function() {
            var msg = "Recuperation des donnees en cours...";
            self.msgs.push($sce.trustAsHtml(msg));
            // self.progressMsg = $sce.trustAsHtml(self.progressMsg + msg);
        };
        self.onGetPricesByCampaignIdComplete = function() {
            self.msgs = [];
            // var msg = "Ok";
            // self.msgs[1] = $sce.trustAsHtml(self.msgs[1] + "Ok");

            //self.msgs.push($sce.trustAsHtml(msg));
            // self.progressMsg = $sce.trustAsHtml(self.progressMsg + msg);
        };

        self.onCancelRun = function() {
            self.msgs = [];
        };

        // $scope.subscribe(events.message._GET_PRICE_BY_CAMPAIGN_ID_,
        //                  self.onGetPricesByCampaignId);
        // $scope.subscribe(events.message._CHECK_PROGRESS_START_,
        //                  self.onCheckProgressStart);
        // $scope.subscribe(events.message._CHECK_PROGRESS_DATA_,
        //                  self.onCheckProgressData);
        // $scope.subscribe(events.message._CHECK_PROGRESS_END_,
        //                  self.onCheckProgressEnd);

        // $scope.subscribe(events.message._GET_PRICE_BY_CAMPAIGN_ID_COMPLETE_,
        //                  self.onGetPricesByCampaignIdComplete);

        $scope.subscribe(events.message._CAMPAIGN_RUN_CANCEL_,
                         self.onCancelRun);

        $scope.subscribe(events.message._CAMPAIGN_CREATE_, self.onCampaignCreate);
        $scope.subscribe(events.message._CAMPAIGN_CREATE_COMPLETE_,
                         self.onCampaignCreateComplete);
    }
})();



    //     // $scope.traceDebug("campaignId " + self.campaignId);
    //     // self.progressMsg = $sce.trustAsHtml('Creation de la consultation...');

    //     // we check progress by intervals
    //     self.campaignCreateHandler = function(campaign) {
    //         $scope.traceDebug('receive CAMPAIGN_CREATE_ID_AVAIL msg <'+campaign.id+'>');
    //         // checkprogress.start(self.campaignId);
    //         self.progressMsg = $sce.trustAsHtml(self.progressMsg +
    //                                             ' OK</br>Collecte en cours, merci de patienter...');
    //         // $scope.publish(events.message._GET_PRICE_BY_CAMPAIGN_ID_, [campaignId]);

    //         $timeout(function(){
    //             checkprogress.start(campaign.id);   
    //         }, 0);
    //         // checkprogress.start();
    //         // /faye/1234');

    //     };
    //     self.updateProgressHandler = function(data) {
    //         $scope.traceDebug('progress bar get new value <' + data + '>');
    //         //toastr SUCCESS progress ' + data);
    //         self.determinateValue = data;
    //         $scope.$digest();
    //         // $timeout(function() { 
    //     };

    //     self.errorProgressHandler = function(data) {
    //         // $scope.traceError('toastr error progress ' + data);
    //         //$scope.$emit('$destroy');
    //     };

    //     self.getPriceByCampaignIdComplete = function() {
    //         self.progressMsg = $sce.trustAsHtml('Creation de la consultation...');
    //     };
        
    //     $scope.subscribe(events.message._GET_PRICE_BY_CAMPAIGN_ID_COMPLETE_,
    //                      self.getPriceByCampaignIdComplete);

    //     $scope.subscribe(events.message._CAMPAIGN_CREATE_ID_AVAILABLE_,
    //                      self.campaignCreateHandler);
    //     $scope.subscribe(events.message._CHECK_PROGRESS_ERROR_,
    //                      self.errorProgressHandler);
    //     $scope.subscribe(events.message._CAMPAIGN_CREATE_FAILED_,
    //                      self.errorProgressHandler);
    //     $scope.subscribe(events.message._CHECK_PROGRESS_DATA_,
    //                      self.updateProgressHandler);
    // }
