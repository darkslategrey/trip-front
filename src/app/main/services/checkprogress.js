(function () {
    'use strict';

    angular.module('app').factory('checkprogress', CheckProgress);


    /** @ngInject */
    function CheckProgress($interval, $log, $http, events, $controller,
                           $rootScope, $mdToast, $timeout,
                           envService)
    {
        var $scope = $rootScope.$new();
        $controller('BaseCtrl', { $scope: $scope });
        $scope.name = 'checkprogress';

        var self = this;

        self.apiUrl      = envService.read('apiUrl');
        self.progressValue = 1;
        self.interval      = undefined;

        self.stopChecking = function() {

            if(angular.isDefined(self.interval)) {
                $interval.cancel(self.interval);
                self.interval = undefined;
            }
        };

        self.success = function(data, status, headers, config) {
            $scope.traceDebug("checkpogress success " + JSON.stringify(data));
            $timeout(function() {
                data = _.has(data, 'result') ? data : { result: [] };
                $scope.publish(events.message._CHECK_PROGRESS_DATA_,
                               [data.result]);
            }, 0);
        };

        self.failure = function(data, status, headers, config) {
            $timeout(function() {
                var msg = 'Problème lors du suivi de progression des acquisitions.';
                // msg += 'Si ce message persiste, merci de recharcher la page';
                data = data || { error:  msg };
                self.stopChecking();
                $scope.publish(events.message._CHECK_PROGRESS_ERROR_,
                               [data]);
            }, 0);
        };

        self.cancelRun = function() {
            self.stopChecking();
        };

        self.progressAction = function() {
            var url =  self.apiUrl + '/campaigns/checkprogress';
            return $http.get(url)
                .success(self.success)
                .error(self.failure);

        };

        self.action = function() {
            var url =  self.apiUrl + '/campaigns/' + self.campaignId + '/checkprogress';
            return $http.get(url)
                .success(self.success)
                .error(self.failure);
        };

        self.delay = 3000;
        // self.count  = 240; // 4 minutes max
        self.count  = 0; // infinite

        self.start  = function() {
            console.log('start checkprogress event');
            if(!angular.isDefined(self.interval)) {
                console.log('go checking');
                self.interval = $interval(self.progressAction, self.delay, self.count);
            } else {
                console.log('checkprogress allready started');
            }
        };

        $scope.subscribe(events.message._CAMPAIGN_RUN_CANCEL_,
                         self.cancelRun);
        $scope.subscribe(events.message._CHECK_PROGRESS_START_,
                         self.start);

        return self;
    }

}());
