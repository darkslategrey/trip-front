angular.module('app').controller('BaseCtrl', function ($scope, $location,
                                                       messaging, events, $log) {
    //#region login methods
    $scope.trace = function (message) {
        messaging.publish(events.message._LOG_TRACE_, [$scope.name + ': ' + message]);
    };

    $scope.traceDebug = function (message) {
        // console.log(message);
        messaging.publish(events.message._LOG_DEBUG_, [$scope.name + ': ' + message]);
    };

    $scope.traceInfo = function (message) {
        messaging.publish(events.message._LOG_INFO_, [$scope.name + ': ' + message]);
    };

    $scope.traceWarning = function (message) {
        messaging.publish(events.message._LOG_WARNING_, [$scope.name + ': ' + message]);
    };

    $scope.traceError = function (message) {
        messaging.publish(events.message._LOG_ERROR_, [$scope.name + ': ' + message]);
    };

    $scope.traceFatal = function (message) {
        messaging.publish(events.message._LOG_FATAL_, [$scope.name + ': ' + message]);
    };
    //#endregion

    //#region messaging vars and methods
    $scope.messagingHandles = [];

    $scope.subscribe = function (topic, callback) {
        var handle = messaging.subscribe(topic, callback);

        if (handle) {
            $scope.messagingHandles.push(handle);
        }
    };

    $scope.publish = function (topic, data) {
        messaging.publish(topic, data);
    };

    $scope.$on('$destroy', function () {
        angular.forEach($scope.messagingHandles, function (handle) {
            // $scope.traceDebug('unsubscribe ' + _.keys(handle));
            messaging.unsubscribe(handle);
        });
    });
    //#endregion

    //#region shared functions
    $scope.onCancel = function () {
        $location.path('/');
    };
    //#endregion
});
