angular.module('app.test').factory('api', function ($q) {
    return {
        campaign: {
            deferred: null,
            all: function () {
                this.deferred = $q.defer();
                return this.deferred.promise;
            },
            get: function (id) {
                this.deferred = $q.defer();
                return this.deferred.promise;
            },
            create: function (obj) {
                this.deferred = $q.defer();
                return this.deferred.promise;
            },
            update: function (obj) {
                this.deferred = $q.defer();
                return this.deferred.promise;
            },
            delete: function (obj) {
                this.deferred = $q.defer();
                return this.deferred.promise;
            }
        }
    };
});


//     return {
//         deferred: null,
//         campaign: {
//             get: function (opts, success, failure) {
//                 this.deferred = $q.defer();
//                 return this.deferred.promise;
//             }
//         }
//     };
// });
