(function () {

    'use strict';

    var Veille = function(params) {
        var self = this;
        var fromSlot =  moment().add(30, 'd');
        var toSlot   = moment().add(30, 'd').add(7, 'd');

        if(_.isEmpty(params)) {
            console.log('params is undefined');
            self.id  = null;
            self.active = true;
            self.notifs = [];
            self.name     = '';
            self.permanent = false;
            self.startDate = moment().toDate();
            self.endDate   = moment().add(30, 'd').toDate();
            self.fromSlot = fromSlot.toDate();
            self.toSlot   = toSlot.toDate();
            self.slots_type = 'absolus';
            self.interval   = '8 heures';
            // self.fromSlot = moment().format('DD/MM/YYYY');
            // self.toSlot   = moment().add(30, 'd').format('DD/MM/YYYY');

            self.proxies   = null;
            self.hotels    = null;
            self.markets   = null;
            self.timestamp = null; // veille's date creatioon / run / collect
            self.prices    = [];
        } else {
            angular.extend(self, params);
            if(!_.has(params, 'fromSlot')) {
                self.fromSlot = fromSlot.toDate();
            }
            if(!_.has(params, 'toSlot')) {
                self.toSlot   = toSlot.toDate();
            }
            if(_.isString(params.fromSlot))  {
                self.fromSlot = moment(params.fromSlot).toDate();
            }
            if(_.isString(params.toSlot))    {
                self.toSlot = moment(params.toSlot).toDate();
            }
            if(_.isString(params.startDate)) {
                self.startDate = moment(params.startDate).toDate();
            }
            if(_.isString(params.endDate)) {
                self.endDate = moment(params.endDate).toDate();
            }
            if(_.isEmpty(params.slots_type)) {
                self.slots_type = 'absolus';
            }
            // self.fromSlot = moment(params.fromSlot).format('DD/MM/YYYY');
            // self.toSlot   = moment(params.toSlot).format('DD/MM/YYYY');
            console.log('params is defined');
        }
    };

    Veille.prototype = {
        computeSomething: function() {
            // we can define custom function
        }
    };

    angular.module('app.models').value('Veille', Veille);
}());
