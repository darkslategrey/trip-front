(function () {

    'use strict';

    var Campaign = function(params) {
        var self = this;
        var fromSlot =  moment().add(30, 'd');
        var toSlot   = moment().add(30, 'd').add(7, 'd');

        if(_.isEmpty(params)) {
            console.log('params is undefined');
            self.id  = null;
            self.name     = '';
            self.progress = '';
            // self.makets = [];
            // self.hotels = [];
            // self.proxies = [];
            self.status = '';
            self.veille = 'n/d';
            self.fromSlot = fromSlot.toDate();
            self.toSlot   = toSlot.toDate();
            self.timestamp = null; // campaign's date creatioon / run / collect
            self.prices    = [];
        } else {
            angular.extend(self, params);
            if(!_.has(params, 'fromSlot')) {
                self.fromSlot = fromSlot.toDate();
            }
            if(!_.has(params, 'toSlot')) {
                self.toSlot   = toSlot.toDate();
            }
            // self.toSlot   = moment(params.toSlot).format('DD/MM/YYYY');
            // self.proxies   = params.proxies;
            // self.hotels    = params.hotels;
            // self.markets   = params.markets;

            console.log('params is defined');
        }
    };

    Campaign.prototype = {
        computeSomething: function() {
            // we can define custom function
        }
    };

    angular.module('app.models').value('Campaign', Campaign);
}());
