(function () {

    /*jshint -W055*/
    angular.module('app').factory('modelTransformer',function() {

        var transformObject = function(jsonResult, constructor) {
            var model = new constructor();
            angular.extend(model, jsonResult);
            // TODO: why these campaigns specifics settings here ???
            model.fromSlot = moment(jsonResult.fromSlot).toDate();
            model.toSlot = moment(jsonResult.toSlot).toDate();
            return model;
        };

        var transformResult = function(jsonResult, constructor) {
            if (angular.isArray(jsonResult)) {
                // console.log("is an array");
                var models = [];
                angular.forEach(jsonResult, function(object) {
                    // console.log("object "+JSON.stringify(object));
                    // console.log("first array");
                    models.push(transformObject(object, constructor));
                });
                return models;
            } else {
                return transformObject(jsonResult, constructor);
            }
        };

        var modelTransformer = {
            transform: transformResult
        };

	      return modelTransformer;
    });

}());
