(function () {

    'use strict';

    var Notif = function(params) {
        var self = this;
        if(_.isEmpty(params)) {
            self.id     = null;
            self.active = true;
            self.bestPriceNotFirst     = true;
            self.officialPriceMissing  = true;
            self.officialPriceNotFirst = true;
            self.dest      = [];
        } else {
            angular.extend(self, params);
        }
    };

    Notif.prototype = {
        computeSomething: function() {
            // we can define custom function
        }
    };

    angular.module('app.models').value('Notif', Notif);
}());
