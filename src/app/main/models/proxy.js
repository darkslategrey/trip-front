(function () {

    'use strict';

    var Proxy = function(params) {
        var self = this;
        if(_.isEmpty(params)) {
            self.id           = null;
            self.ip           = null;
            self.port         = null;
            self.country_name = null;
            self.country_code = null;
            self.location     = null;
        } else {
            angular.extend(self, params);
        }
    };

    Proxy.prototype = {
        computeSomething: function() {
            // we can define custom function
        }
    };

    angular.module('app.models').value('Proxy', Proxy);
}());
