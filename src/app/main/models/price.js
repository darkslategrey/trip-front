(function () {

    'use strict';

    var Price = function() {
        var self = this;
        self.id    = null;
        self.prices = null;
        self.country = null;
        self.hotel = null;
        self.market = null;
        self.campaign_id = null;
        self.error = null;
    };

    Price.prototype = {
        computeSomething: function() {
            // we can define custom function
        }
    };

    angular.module('app.models').value('Price', Price);
}());
