(function ()
{
    'use strict';

    angular
        .module('app.components', ['app', 'app.values'])
        .config(config);

    /** @ngInject */
    function config() {
    }
})();
