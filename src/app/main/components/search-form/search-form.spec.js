// ngDescribe({
//     name: 'select-element',
//     inject: ['$rootScope', '$compile'],
//     modules: 'app.components',
//     tests: function (deps) {
//         it('select all items', function () {
//             var scope = deps.$rootScope.$new();
//             var html = '<search-form campaign="campaign" is-consult-run="isConsultRun" \
//                                      on-submitext="Creer la notification" \
//                                      disablebuttons="disablebuttons" \
//                                      on-submit="onCreateNotif()"></search-form>';
//             var element = angular.element(html);
//             element = deps.$compile(element)(scope);
//             // scope.thing = {name: 'My thing'};
//             scope.$apply();

//             expect(element.find('h1').text()).toBe('');
//         });
//     }
// });

describe('Component: search-form', function () {

    xit('Expect True to be True', function () {
        expect(true).toBe(true);
    });

    beforeEach(function () {
        module('app.components');
        module('generatorGulpAngular');
    });


    var $scope, $rootScope, $compile, $timeout, $componentCtrl, element;

    // beforeEach(inject(function (_$rootScope_, _$compile_, _$timeout_,
    //                             _$componentController_) {
    //     $rootScope  = _$rootScope_;
    //     $compile    = _$compile_;
    //     $timeout    = _$timeout_;
    //     $componentCtrl = _$componentController_;

    //     $scope = $rootScope.$new();

    //     var sfstr = '<search-form campaign="campaign" is-consult-run="isConsultRun" \
    //                          on-submitext="Creer la notification" \
    //                          disablebuttons="disablebuttons" \
    //                          on-submit="onCreateNotif()"></search-form>';
    //     // var sfstr = '<search-form campaign="campaign"
    //     // is-consult-run="isConsultRun" ';
    //     // sfstr     += 'disablebuttons="disablebuttons">';
    //     // sfstr     += '</search-form>';
    //     // element = angular.element('<search-form campaign="campaign"></search-form>');
    //     element = angular.element(sfstr);
    //     $scope.onSubmitext = 'bal';
    //     $scope.campaign = {
    //         fromSlot: moment().format('DD/MM/YYYY'),
    //         toSlot:   moment().add(30, 'd').format('DD/MM/YYYY'),
    //         proxies:  ['0'],
    //         hotels:   ['0'],
    //         markets:  ['0']
    //     };
    //     element = $compile(element)($scope);
    //     $scope.$digest();
    // }));

    describe('with $componentController', function () {

        var controller;
        var scope;
        beforeEach(function() {
            scope = $rootScope.$new();
            controller = $componentCtrl('searchForm',
                                        {$scope: scope},
                                        {
                                            campaign: {
                                                fromSlot: moment().format('DD/MM/YYYY'),
                                                toSlot:   moment().add(30, 'd').format('DD/MM/YYYY'),
                                                proxies:  ['0'],
                                                hotels:   ['0'],
                                                markets:  ['0']
                                            }
                                        });
        });

        xit('should be attached to the scope', function() {
            expect(scope.$ctrl).toBe(controller);
        });

        xit('should expose slots', function() {
            expect(controller.campaign.fromSlot).toBeDefined();
            expect(controller.campaign.toSlot).toBeDefined();
        });
    });

    xit('should compile', function() {
        expect(element.html()).not.toEqual('');
    });

    xit('should config as notifs', function() {
        // TODO: the ng-if is not expanded ?
        expect(element.find('#main-action-nbt').text()).toBe('Creer');
    });
});
