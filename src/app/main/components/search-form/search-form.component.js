(function () {
    'use strict';

    /** ngInject **/
    function SearchFormCtrl($timeout, $rootScope, $controller, $log,
                            events, $state, $scope,
                            Markets, Countries, Hotels) {

        $controller('BaseCtrl', {$scope: $scope});
        $scope.name = 'searchformctrl';

        var vm = this;
        vm.isNotif   = vm.isNotif   || "false";
        vm.isConsult = vm.isConsult || "false";
        vm.isRelatifs = vm.isRelatifs || false;
        vm.showProgressBar = false;
        vm.disableCancel = true;

        $scope.markets = angular.copy(Markets);
        $scope.proxies = angular.copy(Countries);
        $scope.hotels  = angular.copy(Hotels);

        vm.currentCampaignId = false;

        // vm.campaign.markets  = vm.campaign.markets || [vm.markets[0]];
        // vm.campaign.proxies  = vm.campaign.proxies || [vm.proxies[0]];
        // vm.campaign.hotels  = vm.campaign.hotels || [vm.hotels[0]];


        // vm.selectedMarkets = {id: 'all', name: 'Tous'};

        // vm.disablebuttons =  !vm.disablebuttons ? false : true;
        // vm.disablebuttons  = true;
        // vm.isConsultRun    = false;

        vm.selectDimentions = function(selected, dimset) {
            var ids = _.map(selected, function(item) { return item.id; });
            return _.filter(dimset, function(item) {
                return _.findIndex(ids, function(id) { return id == item.id; }) != -1;
            });
        };

        vm.selectDimentionsProxies = function(selected, dimset) {
            var ids = _.map(selected, function(item) { return item.country_code; });
            return _.filter(dimset, function(item) {
                return _.findIndex(ids, function(id) { return id == item.country_code; }) != -1;
            });
        };

        if(_.isEmpty(vm.campaign.markets)) {
            vm.campaign.markets = [{name: 'Tous', id: 'all'}];
        }
        if(_.isEmpty(vm.campaign.hotels)) {
            vm.campaign.hotels = [{name: 'Tous', id: 'all'}];
        }
        if(_.isEmpty(vm.campaign.proxies)) {
            vm.campaign.proxies = [{country_name: 'Tous', country_code: 'all'}];
        }

        $scope.selectedMarkets = vm.selectDimentions(vm.campaign.markets,
                                                     $scope.markets);
        $scope.selectedProxies = vm.selectDimentionsProxies(vm.campaign.proxies,
                                                            $scope.proxies);
        $scope.selectedHotels  = vm.selectDimentions(vm.campaign.hotels,
                                                     $scope.hotels);

        $scope.updateMarketDim = function(selectedMarkets) {
            var indexTousSelected = _.findIndex(selectedMarkets, function(item) {
                return item.name == 'Tous';
            });
            var indexTousCampaign = _.findIndex(vm.campaign.markets, function(item) {
                return item.name == 'Tous';
            });
            var sizeOfSelected = selectedMarkets.length;

            if(indexTousSelected != -1) {
                if(indexTousCampaign == -1) {
                    $scope.selectedMarkets = [$scope.markets[0]];
                    // vm.campaign.markets = $scope.selectedMarkets;
                } else {
                    _.remove($scope.selectedMarkets, function(item) {
                        return item.name == 'Tous';
                    });
                    // vm.campaign.markets = $scope.selectedMarkets;
                }
            } else {
                if($scope.selectedMarkets.length == $scope.markets.length - 1) {
                    $scope.selectedMarkets = [$scope.markets[0]];
                    // vm.campaign.markets = $scope.selectedMarkets;
                }
            }
            vm.campaign.markets = $scope.selectedMarkets;
        };

        $scope.updateProxieDim = function(selectedProxies) {
            var indexTousSelected = _.findIndex(selectedProxies, function(item) {
                return item.country_name == 'Tous';
            });
            var indexTousCampaign = _.findIndex(vm.campaign.proxies, function(item) {
                return item.country_name == 'Tous';
            });
            var sizeOfSelected = selectedProxies.length;

            if(indexTousSelected != -1) {
                if(indexTousCampaign == -1) {
                    $scope.selectedProxies = [$scope.proxies[0]];
                    // vm.campaign.proxies = selectedProxies;
                } else {
                    _.remove($scope.selectedProxies, function(item) {
                        return item.country_name == 'Tous';
                    });
                    // vm.campaign.proxies = selectedProxies;
                }
            } else {
                if($scope.selectedProxies.length == $scope.proxies.length - 1) {
                    $scope.selectedProxies = [$scope.proxies[0]];
                    // vm.campaign.proxies = selectedProxies;
                }
            }
            vm.campaign.proxies = $scope.selectedProxies;
            // $scope.traceDebug("vm campaign selected proxies " +
            //                   JSON.stringify(vm.campaign.proxies));
        };

        $scope.$on('$destroy', function () {
            // $scope.traceDebug("on destroy");
            // $scope.traceDebug("vm campaign selected proxies " +
            //                   JSON.stringify(vm.campaign.proxies));
        });

        $scope.updateHotelDim = function(selectedHotels) {
            var indexTousSelected = _.findIndex(selectedHotels, function(item) {
                return item.name == 'Tous';
            });
            var indexTousCampaign = _.findIndex(vm.campaign.hotels, function(item) {
                return item.name == 'Tous';
            });
            var sizeOfSelected = selectedHotels.length;

            if(indexTousSelected != -1) {
                if(indexTousCampaign == -1) {
                    $scope.selectedHotels = [$scope.hotels[0]];
                    // vm.campaign.hotels = $scope.selectedHotels;
                } else {
                    _.remove($scope.selectedHotels, function(item) {
                        return item.name == 'Tous';
                    });
                    // vm.campaign.hotels = $scope.selectedHotels;
                }
            } else {
                if($scope.selectedHotels.length == $scope.hotels.length - 1) {
                    $scope.selectedHotels = [$scope.hotels[0]];
                    //vm.campaign.hotels = $scope.selectedHotels;
                }
            }
            vm.campaign.hotels = $scope.selectedHotels;
        };

        vm.showButtons = function() {
            var yesno = vm.isConsult === "true" ? true : false;
            // $scope.traceDebug("yesno <"+yesno+">");
            return yesno;
        };

        vm.onCancel  = function() {
            // $scope.traceDebug("SearchFormCtrl Cancel clicked");
            vm.isConsultRun   = false;
            vm.disablebuttons = true;
            $scope.publish(events.message._CAMPAIGN_RUN_CANCEL_,[vm.campaign.id]);
            // $scope.$digest();
            // $scope.$emit('$destroy');
        };
        vm.onConsult = function() {
            // $scope.traceDebug('search form compoennt click !');
            // $scope.traceDebug('vm.campaign !' + JSON.stringify(vm.campaign));
            // $scope.traceDebug('selectedProxies !' + JSON.stringify($scope.selectedProxies));
            vm.campaign.hotels  = $scope.selectedHotels;
            vm.campaign.markets = $scope.selectedMarkets;
            vm.campaign.proxies = $scope.selectedProxies;
            vm.isConsultRun   = true;
            vm.onSubmit({campaign: vm.campaign});
        };

        if(_.isString(vm.campaign.fromSlot)) {
            var from = moment(new Date(vm.campaign.fromSlot)).format('d/m/Y');
            var to = moment(new Date(vm.campaign.toSlot)).format('d/m/Y');
            vm.campaign.fromSlot = from;
            vm.campaign.toSlot   = to;
        }
        // vm.campaign.fromSlot = new Date(moment(vm.campaign.fromSlot, 'DD/MM/YYYY'));
        // vm.campaign.toSlot   = new Date(moment(vm.campaign.toSlot, 'DD/MM/YYYY'));
        // }

        vm.errorCreate = function() {
            // $scope.traceDebug('search form ERROR create');
            vm.isConsultRun = false;
            vm.disablebuttons = true;
            vm.showProgressBar = false;
            // $scope.$digest();
        };
        vm.onCreateNotif = function() {
            // $scope.traceDebug('onCreateNotif');
            // $scope.traceDebug(vm.campaign.fromSlot);
            vm.campaign.proxies = vm.selectedCountries;
            vm.campaign.markets = vm.selectedMarkets;
            vm.campaign.hotels  = vm.selectedHotels;

            // $scope.traceDebug('proxies ' + JSON.stringify(vm.campaign.proxies));
            // $scope.traceDebug('markets ' + JSON.stringify(vm.campaign.markets));
            // $scope.traceDebug('hotels ' + JSON.stringify(vm.campaign.hotels));

            $state.go('app.notifs', {campaign: vm.campaign});
        };
        vm.onCreateVeille = function() {
            // $scope.traceDebug('onCreateVeille');
            // $scope.traceDebug(vm.campaign.fromSlot);
            $state.go('app.veille', {veille: vm.campaign});
        };

        vm.successCreate = function(data) {
            // $scope.traceDebug('search form SUCCESS create '+ JSON.stringify(data));
            $scope.selectedMarkets = [{name: 'Tous', id: 'all'}];
            $scope.selectedHotels = [{name: 'Tous', id: 'all'}];
            $scope.selectedProxies = [{country_name: 'Tous', country_code: 'all'}];
            // $scope.traceDebug('search form SUCCESS vm.campaign AVANT '+ JSON.stringify(vm.campaign));
            // angular.extend(vm.campaign, data);
            // $scope.traceDebug('search form SUCCESS vm.campaign APRES '+ JSON.stringify(vm.campaign));
            // vm.currentCampaignId = $scope.campaign.id;

            // // vm.isConsultRun = false;
            // //vm.disablebuttons = false;
            vm.disablebuttons = true;
            vm.disableCancel  = false;
        };


        $scope.subscribe(events.message._CAMPAIGN_CREATE_COMPLETE_,
                         vm.successCreate);
        $scope.subscribe(events.message._CAMPAIGN_CREATE_FAILED_,
                         vm.errorCreate);
        // $scope.traceDebug('Consult Run ' + vm.isConsultRun);
        // $scope.traceDebug('Is consult ' + vm.isConsult);
        // $scope.traceDebug('Is notif ' + vm.isNotif);
        // $scope.traceDebug('markets ' + JSON.stringify(Markets));
        // $scope.traceDebug('hotels ' + JSON.stringify(Hotels));
        // $scope.traceDebug('proxies ' + JSON.stringify(Countries));
        // $scope.traceDebug('selectedMarkets ' + JSON.stringify($scope.selectedMarkets));
        // $scope.traceDebug('selectedProxies ' + JSON.stringify($scope.selectedProxies));
        // $scope.traceDebug('selectedHotels ' + JSON.stringify($scope.selectedHotels));

        // $scope.traceDebug('campaignMarkets ' + JSON.stringify(vm.campaign.markets));
        // $scope.traceDebug('campaignProxies ' + JSON.stringify(vm.campaign.proxies));
        // $scope.traceDebug('campaignHotels ' + JSON.stringify(vm.campaign.hotels));

    }
    angular
        .module('app.components')
        .component('searchForm', {
            templateUrl: 'app/main/components/search-form/search-form.html',
            bindings: {
                isNotif: '@',
                onSubmitext: '@',
                campaign: '<',
                disablebuttons: '=',
                isConsultRun: '=',
                isConsult: '@',
                isRelatifs: '<',
                onSubmit: '&'
            },
            controller: SearchFormCtrl
        })
        .config(config);


    /** @ngInject */
    function config($mdDateLocaleProvider) {
        $mdDateLocaleProvider.formatDate = function(date) {
            return moment(date).format('DD/MM/YYYY');
        };
    }

})();
