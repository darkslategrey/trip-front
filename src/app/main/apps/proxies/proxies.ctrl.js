(function ()
 {
     'use strict';

     angular
         .module('app.proxies')
         .controller('ProxiesCtrl', ProxiesCtrl);

     /** @ngInject */
     function ProxiesCtrl($controller, $scope, api, events, $mdToast,
                          proxiesCache, DTOptionsBuilder, DTColumnDefBuilder,
                          Proxy, $mdDialog, $document, $http)
     {
         var vm = this;

         $controller('BaseCtrl', {$scope: $scope});

         $scope.name = 'ProxiesCtrl';
         $scope.csv = {
             content: '',
             header: true,
             headerVisible: false,
             result: '',
             separator: ';',
             encodingVisible: false,
             separatorVisible: true,
             encoding: 'ISO-8859-1',
             accept: ''
         };

         vm.dtOptions = DTOptionsBuilder.newOptions()
             .withOption('order', [1, 'asc'])
             .withOption('dom', 'Bfrtip')
             .withOption('responsive', true)
             .withPaginationType('simple')
             .withButtons([{
                 extend: 'csv',
                 text: 'Exporter en CSV',
                 filename: 'proxies',
                 fieldSeparator: $scope.csv.separator
             }]);

         vm.dtColumnDefs = [
             DTColumnDefBuilder.newColumnDef(0),
             DTColumnDefBuilder.newColumnDef(1),
             DTColumnDefBuilder.newColumnDef(2),
             DTColumnDefBuilder.newColumnDef(3),
             DTColumnDefBuilder.newColumnDef(4),
             DTColumnDefBuilder.newColumnDef(5)
         ];

         $http({
             method: 'GET',
             url: '/api/proxies'
         }).then(function(response) {
             $scope.traceDebug('success get all proxies ' + JSON.stringify(response));
             $scope.proxies = response.data;
             // this callback will be called asynchronously
             // when the response is available
         }, function(response) {
             $scope.traceDebug('error get all proxies ' + JSON.stringify(response));
             var msg = 'Erreur: Liste des proxies';
             $mdToast.show($mdToast.simple()
                           .position('top right')
                           .highlightClass('md-warn')
                           .highlightAction(true)
                           .hideDelay(0)
                           .action('OK')
                           .textContent(msg));
         });

         // if(!_.isArray(proxies)) {
         //     $mdToast.show($mdToast.simple()
         //                   .position('top right')
         //                   .highlightClass('md-warn')
         //                   .highlightAction(true)
         //                   .hideDelay(0)
         //                   .action('OK')
         //                   .textContent(proxies.msg));
         // } else {
         //     $scope.proxies = proxies;
         // }

         // $scope.traceDebug("Proxies : " + JSON.stringify($scope.proxies));

         vm.activateProgress = false;
         vm.proxy = new Proxy();
         vm.addProxy =  function(e, type, proxy) {
             showEventFormDialog(type, proxy, e);
         };

         vm.removeProxyConfirm = function(proxyId, e) {
             var confirm = $mdDialog.confirm({skipHide: true})
                 .title('Êtes-vous sûr ?')
                 .ariaLabel('Supprimer le proxy')
                 .targetEvent(e)
                 .ok('OK')
                 .cancel('Annuler');

             $mdDialog.show(confirm).then(function () {
                 vm.removeProxy(proxyId);
             }, function () {});
         };

         vm.removeProxy = function(proxyId) {
             $scope.publish(events.message._PROXY_REMOVE_, [proxyId]);
         };

         function showEventFormDialog(type, proxy, e)
         {
             $scope.traceDebug('here the type ' + type);
             var dialogData = {
                 type         : type,
                 proxy       : proxy
             };

             $mdDialog.show({
                 controller         : 'ProxyFormDialogCtrl',
                 controllerAs       : 'vm',
                 templateUrl        : 'app/main/apps/proxies/dialogs/details.html',
                 parent             : angular.element($document.body),
                 targetEvent        : e,
                 clickOutsideToClose: true,
                 locals             : {
                     dialogData: dialogData
                 }
             }).then(function (response) {
                 $scope.traceDebug("close dialog " + response.type);

                 if(response.type == 'add') {
                     $scope.traceDebug("go to publish proxy CREATE");
                     $scope.traceDebug("proxy " + JSON.stringify(response.proxy));
                     vm.currentCreatedProxy = angular.copy(response.proxy);
                     $scope.publish(events.message._PROXY_CREATE_, [response.proxy]);
                 } else {
                     $scope.traceDebug("go to publish proxy UPDATE");
                     vm.selectedProxyIdx = _.findIndex($scope.proxies, function(proxy) {
                         return proxy.id == response.proxy.id;
                     });
                     vm.currentUpdatedProxy = angular.copy(response.proxy);
                     $scope.publish(events.message._PROXY_UPDATE_, [response.proxy]);
                 }
             });
         }
         vm.update = function() {
             // [{"0":"200.43.219.67","1":"3128","2":"Argentina"},
             // {"0":"138.68.49.86","1":"8080","2":"United States"}]
             // $scope.traceDebug("csv content " + JSON.stringify($scope.csv.content));
             $scope.traceDebug("csv result " + JSON.stringify($scope.csv.result));
             vm.activateProgress = true;
             $scope.publish(events.message._PROXIES_UPDATE_, [$scope.csv.result]);
         };

         vm.stopProgress = function() {
             setTimeout(function() {
                 $scope.$apply(function() {
                     vm.activateProgress = false;
                 });
             }, 0);
         };

         vm.onProxiesUpdateComplete = function(proxies){
             $scope.traceDebug('update complete ' + JSON.stringify(proxies));
             vm.stopProgress();
             proxiesCache.reset();
             $scope.proxies = proxies; // $scope.csv.result;
         };

         vm.onProxiesUpdateFailed = function(data){
             $scope.traceDebug("onProxiesUpdateFailed: " + JSON.stringify(data));
             vm.stopProgress();
             $mdToast.show($mdToast.simple()
                           .position('top right')
                           .highlightClass('md-warn')
                           .highlightAction(true)
                           .hideDelay(3000)
                           .action('OK')
                           .textContent("Échec de la mise à jour"));
         };

         vm.onProxyCreateComplete = function(proxyId) {
             vm.currentCreatedProxy.id = proxyId;
             var newProxy = new Proxy(vm.currentCreatedProxy);
             $scope.proxies.push(angular.copy(newProxy));
             $mdToast.show($mdToast.simple()
                           .position('top right')
                           .highlightClass('md-warn')
                           .highlightAction(true)
                           .hideDelay(3000)
                           .action('OK')
                           .textContent('Proxy ajouté'));
             vm.currentCreatedProxy = null;
         };

         vm.onProxyCreateFailed = function() {
             $mdToast.show($mdToast.simple()
                           .position('top right')
                           .highlightClass('md-warn')
                           .highlightAction(true)
                           .hideDelay(3000)
                           .action('OK')
                           .textContent('Erreur: Proxy non ajouté'));
             vm.currentCreatedProxy = null;
         };

         vm.onProxyUpdateComplete = function(data) {
             $scope.proxies[vm.selectedProxyIdx] = vm.currentUpdatedProxy;
             $mdToast.show($mdToast.simple()
                           .position('top right')
                           .highlightClass('md-warn')
                           .highlightAction(true)
                           .hideDelay(3000)
                           .action('OK')
                           .textContent('Proxy mis à jour'));
             vm.currentUpdatedProxy = null;
         };

         vm.onProxyUpdateFailed = function() {
             $mdToast.show($mdToast.simple()
                           .position('top right')
                           .highlightClass('md-warn')
                           .highlightAction(true)
                           .hideDelay(3000)
                           .action('OK')
                           .textContent('Erreur: Proxy non mis à jour'));
             vm.currentUpdatedProxy = null;
         };

         vm.onProxyRemoveComplete = function(proxyId) {
             $scope.traceDebug("remove data " + JSON.stringify(proxyId));
             $mdToast.show($mdToast.simple()
                           .position('top right')
                           .highlightClass('md-warn')
                           .highlightAction(true)
                           .hideDelay(3000)
                           .action('OK')
                           .textContent('Proxy supprimé'));

             _.remove($scope.proxies, function(proxy) {
                 $scope.traceDebug("proxy.id " + proxy.id);
                 $scope.traceDebug("data.id " + proxyId);
                 return proxy.id == proxyId;
             });
         };

         $scope.subscribe(events.message._PROXIES_UPDATE_COMPLETE_,
                          vm.onProxiesUpdateComplete);
         $scope.subscribe(events.message._PROXIES_UPDATE_FAILED_,
                          vm.onProxiesUpdateFailed);

         $scope.subscribe(events.message._PROXY_CREATE_COMPLETE_,
                          vm.onProxyCreateComplete);
         $scope.subscribe(events.message._PROXY_CREATE_FAILED_,
                          vm.onProxyCreateFailed);

         $scope.subscribe(events.message._PROXY_UPDATE_COMPLETE_,
                          vm.onProxyUpdateComplete);
         $scope.subscribe(events.message._PROXY_UPDATE_FAILED_,
                          vm.onProxyUpdateFailed);

         $scope.subscribe(events.message._PROXY_REMOVE_COMPLETE_,
                          vm.onProxyRemoveComplete);
         $scope.subscribe(events.message._PROXY_REMOVE_FAILED_,
                          vm.onProxyRemoveFailed);


     }

 })();
