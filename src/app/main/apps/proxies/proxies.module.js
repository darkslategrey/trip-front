(function ()
 {
     'use strict';

     angular
         .module('app.proxies', ['app.components', 'app', 'app.core', 'app.models',
                                 'app.dataservices', 'app.cacheservice',
                                 'datatables.buttons'])
         .config(config);

     /** @ngInject */
     function config($stateProvider, msNavigationServiceProvider)
     {
         // State
         $stateProvider.state('app.proxies', {
             url      : '/proxies',
             views    : {
                 'content@app': {
                     templateUrl: 'app/main/apps/proxies/proxies.html',
                     controller : 'ProxiesCtrl as vm'
                     // resolve:  {
                     //     proxies: function(proxiesCache, api) {
                     //         return api.proxy.all();
                     //         // return proxiesCache.all();
                     //     }
                     // }
                 }
             }
         });

         // Navigation
         msNavigationServiceProvider.saveItem('apps.proxies', {
             title : 'Proxies',
             icon  : 'icon-earth',
             state : 'app.proxies',
             weight: 2
         });
     }

 })();
