(function () {

    'use strict';

    angular
        .module('app.proxies')
        .controller('ProxyFormDialogCtrl', ProxyFormDialogCtrl);

    /** @ngInject */
    function ProxyFormDialogCtrl($mdDialog, dialogData, $controller,
                                 $document, $scope, Proxy, Countries) {
        $controller('BaseCtrl', {$scope: $scope});
        var vm = this;

        $scope.countries  = angular.copy(_.filter(Countries, function(country) {
            return country.country_name != 'Tous';
        }));

        vm.dialogData = dialogData;
        $scope.name = 'ProxyFormDialogCtrl';

        // Methods
        vm.saveProxy = saveProxy;
        vm.closeDialog = closeDialog;


        init();

        function init()
        {
            $scope.dialogTitle = (vm.dialogData.type === 'add' ? 'Ajouter une Proxy' : 'Modifier une Proxy');
            if ( vm.dialogData.proxy ) {
                // Edit
                $scope.traceDebug("go to edit");
                var selectedCountry = _.find($scope.countries, function(country) {
                    return country.country_code == vm.dialogData.proxy.country_code;
                });
                vm.proxy = new Proxy(vm.dialogData.proxy);
                vm.proxy.country_name = selectedCountry.country_name;
            } else {
                // Add
                $scope.traceDebug("go to create");
                vm.proxy= new Proxy({});
            }
        }

        function saveProxy()
        {
            var country = _.filter(Countries,
                                   { country_name: vm.proxy.country_name })[0];
            vm.proxy.country_code = country.country_code;
            var response = {
                type: vm.dialogData.type,
                proxy: vm.proxy
            };
            $mdDialog.hide(response);
        }
        /**
         * Close the dialog
         */
        function closeDialog()
        {
            $mdDialog.cancel();
        }
    }

}());
