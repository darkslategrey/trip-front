describe('consult-table-ctrl', function(){
    it('Expect True to be True', function () {
        expect(true).toBe(true);
    });


    beforeEach(function () {
        module('app');
        module('app.consult');
        module('generatorGulpAngular');
    });
    var $scope, $rootScope, $compile, $timeout, $componentCtrl, element;
    beforeEach(inject(function (_$rootScope_, _$compile_, _$timeout_,
                                _$componentController_) {
        $rootScope  = _$rootScope_;
        $compile    = _$compile_;
        $timeout    = _$timeout_;
        $componentCtrl = _$componentController_;
        $scope = $rootScope.$new();
        var bindings = {};

        controller = $componentCtrl('consultTable',
                                    {$scope: $scope},
                                    bindings);

    }));
});
//     var $scope, $rootScope, $controller, messaging, events, $log,
//         $compile, searchformElt, $httpBackend, api, pricesFormatter,
//         $sce;

//     // afterEach(function() {
//     //     $httpBackend.verifyNoOutstandingExpectation();
//     //     $httpBackend.verifyNoOutstandingRequest();
//     // });

//     beforeEach(inject(function (_$controller_, _$rootScope_,
//                                 _messaging_, _events_, _$log_, _api_,
//                                 _$compile_, _$httpBackend_, _$filter_,
//                                _$sce_) {
//         $controller = _$controller_;
//         $rootScope  = _$rootScope_;
//         messaging   = _messaging_;
//         events      = _events_;
//         $log        = _$log_;
//         $compile    = _$compile_;
//         api         = _api_;
//         $sce        = _$sce_;

//         pricesFormatter = _$filter_('pricesFormatter');
//         $httpBackend = _$httpBackend_;
//         $httpBackend.when('POST', '/api/campaigns')
//             .respond({data: {name: 'test campaign'}}); // value: 40});

//         var sfstr = '<search-form campaign="campaign" is-consult-run="isConsultRun" ';
//         sfstr     += 'disablebuttons="disablebuttons">';
//         sfstr     += '</search-form>';

//         searchformElt = angular.element(sfstr);
//     }));

//     afterEach(function() {
//         console.log($log.debug.logs);
//         console.log($log.log.logs);
//         console.log($log.error.logs);
//     });

//     it('should format prices', function() {
//         var expected = '<ol><li>TripAdvisor: 142</li><li>Official Site: 126</li>';
//         expected += '</ol>';
//         var prices = { "pprices": [
//                 {
//                     "name": "TripAdvisor",
//                     "price": 142,
//                     "position": 1
//                 },
//                 {
//                     "name": "Official Site",
//                     "price": 126,
//                     "position": 2
//                 }
//             ]
//         };
//         expect($sce.getTrustedHtml(pricesFormatter(prices))).toEqual(expected);
//     });

//     xit('can be instantiated', function() {
//         expect($rootScope.$new()).toBeDefined();
//         $scope = $rootScope.$new();
//         var ctrl =  $controller('ConsultCtrl', { $scope: $scope });
//         expect(ctrl).not.toBe(null);
//         expect($scope.campaign.fromSlot).toEqual(moment().format('DD/MM/YYYY'));
//         expect($scope.campaign.toSlot).toEqual(moment().add(30,
//                                                             'd').format('DD/MM/YYYY'));
//     });

//     it('it publish message create message', function() {
//         spyOn(messaging, 'publish').and.callThrough();
//         $scope = $rootScope.$new();
//         // $scope.messaging = messaging;
//         var ctrl =  $controller('ConsultCtrl', { $scope: $scope });
//         $scope.onConsult();
//         // $httpBackend.flush();
//         expect(messaging.publish).toHaveBeenCalled();
//         expect(messaging.publish.calls.count()).toEqual(3);
//     });

//     it('should show the progress bar when click consult', function() {
//         $scope = $rootScope.$new();
//         // $scope.disablebuttons = false;
//         $scope.campaign = {
//             name: 'campagne 1',
//             fromSlot: moment().format('DD/MM/YYYY'),
//             toSlot:   moment().add(30, 'd').format('DD/MM/YYYY'),
//             proxies:  ['0'],
//             hotels:   ['0'],
//             markets:  ['0']
//         };
//         searchformElt     = $compile(searchformElt)($scope);
//         // $scope.$apply();
//         $controller('ConsultCtrl', { $scope: $scope });
//         // expect(searchformElt.attr('class')).toMatch(/ng-hide/);
//         expect($scope.isConsultRun).toBeFalsy();
//         $scope.onConsult();

//         // expect(searchformElt.find('#consult-btn')).not.toBeUndefined();

//         // var res = {data: [{id: 1, name: 'c1'}, {id: 2, name: 'c2'} ]};
//         // expect(_.keys(api.campaign.deferred)).toEqual('');
//         // api.campaign.deferred.resolve(res);
//         // $rootScope.$digest();

//         // $scope.$digest();
//         // $httpBackend.expectPOST('/api/campaigns');
//         // $httpBackend.flush();
//         // expect($scope.isConsultRun).toBeTruthy();
//         // $scope.$digest();
//         // expect(searchformElt.attr('class')).not.toMatch(/ng-hide/);
//     });


// });
