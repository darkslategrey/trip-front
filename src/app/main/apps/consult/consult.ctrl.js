(function ()
 {
   'use strict';
   // http://www.summa.com/blog/angular-ui-router-an-elegant-maintainable-way-to-prevent-state-change
   angular
     .module('app.consult')
     .controller('ConsultCtrl', ConsultCtrl);

   angular.module('app.consult')
     .filter('pricesFormatter', PricesFormatter);

   angular.module('app.consult')
     .filter('idFilter', idFilter);

   angular.module('app.consult')
     .filter('dateFormatter', dateFormatter);

   angular.module('app.consult')
     .filter('timeStampFormatter', timeStampFormatter);

   angular.module('app.consult')
     .filter('dimFormatter', dimFormatter);

   angular.module('app.consult')
     .filter('proxyDimFormatter', proxyDimFormatter);

   function idFilter() {
     return function(id) {
       console.log("this is the id <" + id + ">");
       return id.substring(0, 5);
     };
   }

   function dimFormatter($sce) {
     return function(items) {
       var out = '';
       if(_.isEmpty(items)) {
         out = 'n/a';
       } else {
         // out = '<ul>';
         _.forEach(items, function(item){
           if(item.name == 'all') {
             out += 'Tous</br>';
           } else {
             out += item.name + '<br/>';
           }
         });
         // out += '</ul>';
       }
       return $sce.trustAsHtml(out);
     };
   }
   function proxyDimFormatter($sce) {
     return function(items) {
       var out = '';
       if(_.isEmpty(items)) {
         out = 'n/a';
       } else {
         // out = '<ul>';
         _.forEach(items, function(item){
           if(item.country_code == 'all') {
             out += 'Tous</br>';
           } else {
             out += item.country_name + '<br/>';
           }
         });
         // out += '</ul>';
       }
       return $sce.trustAsHtml(out);
     };
   }

   function timeStampFormatter() {
     return function(ts) {
       return moment(ts).format('DD/MM/YYYY - HH:mm');
     };
   }

   function dateFormatter() {
     return function(date) {
       return moment(date).format('DD/MM/YYYY');
     };
   }

   /** @ngInject */
   function PricesFormatter($sce) {
     return function(prices) {
       var out = '<ol>';
       _.forEach(prices, function(priceObj){
         var broker = priceObj.name;
         var price  = priceObj.price;
         out += '<li>' + broker + ': ' + price + '</li>';
       });
       out += '</ol>';
       if(prices == 'not matched' || _.isEmpty(prices)) {
         out = 'Non disponible';
       }
       return $sce.trustAsHtml(out);
     };
   }

   /** @ngInject */
   function ConsultCtrl($scope, Campaign, events, $controller, api,
                        $log, $mdToast, $timeout, $document,
                        FileSaver, Blob, $filter, $http,
                        Markets, Hotels, Countries, $cacheFactory, DTOptionsBuilder,
                        modelTransformer, $mdDialog)
   {
     $controller('BaseCtrl', {$scope: $scope});
     var vm = this;
     // var Campaign, messaging, api;

     var pricesCache = $cacheFactory.get('prices') || $cacheFactory('prices');
     var latestPrices = [];

     if(!angular.isUndefined(pricesCache.get('latest'))) {
       latestPrices = pricesCache.get('latest');
     }

     // $scope.campaign     = new Campaign({prices: latestPrices});
     $scope.campaigns = [];

     // api.campaign.all().then(function(response) {
     //   $scope.campaigns = response;
     //   // $scope.traceDebug('success get all campaigns ' + JSON.stringify($scope.campaigns));
     // }, function(response) {
     //   $scope.traceDebug('error get all campaigns ' + JSON.stringify(response));
     //   var msg = 'Erreur: Liste des acquisitions indisponible';
     //   $mdToast.show($mdToast.simple()
     //                 .position('top right')
     //                 .highlightClass('md-warn')
     //                 .highlightAction(true)
     //                 .hideDelay(0)
     //                 .action('OK')
     //                 .textContent(msg));
     // });

     // api.campaign.all().then(function(response){
     //     $scope.traceDebug('api campaigns gget all ' + JSON.stringify(response));

     // });
     $scope.newCampaign     = new Campaign({});
     $scope.isConsultRun = false;
     $scope.disablebuttons = false;
     $scope.showProgress = false;
     $scope.disablePrices   = true;
     $scope.selectedIndex   = 0;
     $scope.disableConsult   = true;
     $scope.name = 'consultctrl';

     // if(_.isEmpty(campaigns.length)) {
     //     $scope.campaigns = [];
     // }

     // if(campaigns.length == 1 && _.has(campaigns[0], 'error')) {
     //     $scope.traceDebug('error get /api/campaigns');
     //     $mdToast.show($mdToast.simple()
     //                   .position('top right')
     //                   .highlightClass('md-warn')
     //                   .highlightAction(true)
     //                   .hideDelay(0)
     //                   .action('OK')
     //                   .textContent('Erreur: Liste des acquisitions indisponible'));
     // } else {
     //     $scope.campaigns = campaigns;
     // }

     if(_.isString($scope.newCampaign.fromSlot)) {
       $scope.newCampaign.fromSlot = new Date(moment($scope.newCampaign.fromSlot,
                                                     'DD/MM/YYYY'));
       $scope.newCampaign.toSlot   = new Date(moment($scope.newCampaign.toSlot,
                                                     'DD/MM/YYYY'));
     }
     vm.showPrices = function(campaign) {
       $scope.showProgress = true;
       // blockUI.message("Chargement en cours...");
       $scope.traceDebug("go to prices for ");
       $scope.traceDebug(" campaign id "+ campaign.id);
       $scope.publish(events.message._GET_PRICE_BY_CAMPAIGN_ID_, [campaign.id]);
     };

     // DELETE CAMPAIGN
     vm.onRemoveCampaignConfirm = function(campaignId) {

       $scope.traceDebug(' onRemoveCampaignConfirm  ' +
                         JSON.stringify(campaignId));
       var confirm = $mdDialog.confirm({skipHide: true})
           .title('Êtes-vous sûr ?')
           .ariaLabel('Supprimer la campagne')
           .ok('OK')
           .cancel('Annuler');

       $mdDialog.show(confirm).then(function () {
         vm.removeCampaign(campaignId);
       }, function () {});
     };

     vm.removeCampaign = function(campaignId) {
       $scope.traceDebug("go to remove campaign "+campaignId);
       vm.showProgress = true;
       api.campaign.delete(campaignId).then(vm.successDestroyCampaign,
                                            vm.errorDestroyCampaign);
     };

     vm.successDestroyCampaign = function(response) {
       $scope.traceDebug("SUCCES destroy campaign " + JSON.stringify(response));
       if (response.data) {
         $scope.traceDebug("SUCCES go remove campaign ");
         vm.showProgress = false;
         var campaignId = response.data.campaignId;
         $scope.traceDebug("campaigns before " + JSON.stringify($scope.campaigns));
         var idx = _.findIndex($scope.campaigns, { 'id': campaignId });
         $scope.campaigns.splice(idx, 1);
         $scope.traceDebug("campaigns after " + JSON.stringify($scope.campaigns));
       } else {
         vm.errorDestroyCampaign();
       }
     };

     vm.$onInit = function() {
       $scope.traceDebug('INIT before all consult ctrl ' + $scope.campaigns.length);
       $scope.traceDebug('$scope.camapigns ' + JSON.stringify($scope.campaigns));
       api.campaign.all().then(vm.successAllCampaigns, vm.failedAllCampaigns);
     };

     vm.successAllCampaigns = function(response) {
       $scope.traceDebug('success all campaigns ' + response.length);
       $scope.campaigns = response;
     };
     vm.failedAllCampaigns = function(response) {
       $scope.traceDebug('failed all campaigns' + JSON.stringify(response));
       $mdToast.show($mdToast.simple()
                     .position('top right')
                     .highlightClass('md-warn')
                     .highlightAction(true)
                     .hideDelay(2000)
                     .action('OK')
                     .textContent('Erreur: Liste des acquisitions indisponible'));
     };

     vm.$onDestroy = function() {
       $scope.traceDebug('DESTROY consult ctrl');
       $scope.traceDebug('$scope.camapigns ' + JSON.stringify($scope.campaigns));
     };
     vm.errorDestroyCampaign = function () {
       $scope.traceDebug("ERROR destroy campaign " + JSON.stringify(response));
       vm.showProgress = false;
       $mdToast.show($mdToast.simple()
                     .position('top right')
                     .highlightClass('md-warn')
                     .highlightAction(true)
                     .hideDelay(0)
                     .action('OK')
                     .textContent("Cannot delete campaign"));
     };


     // REFRESH
     vm.refreshTestsList = function() {
       $scope.traceDebug('go to refresh tests list');
       $scope.showProgress = true;
       var timestamps = _.map($scope.campaigns, 'timestamp');
       $scope.traceDebug('campaign timestamps ' + timestamps);
       api.campaign.refresh(timestamps).then(vm.successRefresh, vm.failedRefresh);
     };

     vm.successRefresh = function(response) {
       $scope.traceDebug("success refresh");
       $scope.showProgress = false;
       // $scope.traceDebug("response " + JSON.stringify(response));
       if(_.isEmpty(response)) {
         $mdToast.show($mdToast.simple()
                       .position('top right')
                       .highlightClass('md-warn')
                       .highlightAction(true)
                       .hideDelay(400)
                       .action('OK')
                       .textContent("Pas de nouveau tests"));
       }
       angular.forEach(response, function (camp) {
         $scope.campaigns.push(modelTransformer.transform(camp, Campaign));
       });
     };
     vm.failedRefresh = function(response) {
       $scope.traceDebug("Failure refresh");
       $scope.showProgress = false;
       var errormsg = 'Erreur lors de la mise à jour de la liste des tests';
       $mdToast.show($mdToast.simple()
                     .position('top right')
                     .highlightClass('md-warn')
                     .highlightAction(true)
                     .hideDelay(0)
                     .action('OK')
                     .textContent(errormsg));
     };

     //     $scope.currentCampaign = campaign;
     //     // $scope.traceDebug(" prices "+ JSON.stringify(campaign.prices));
     //     // $scope.campaign = campaign;
     //     $scope.selectedIndex = 1;
     // };

     var domOptions = '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i>';
     domOptions += '<"pagination"p>>>';


     vm.dtOptions = DTOptionsBuilder.newOptions()
       .withOption('dom', domOptions)
       .withOption('responsive', true)
       .withOption('bAutoWidth', false)
       .withOption('deferRender', true)
       .withPaginationType('full_numbers')
       .withLightColumnFilter({
         '0' : { html: '', type: null, width: '100%' },
         '1' : {
           html : 'select',
           width: '100%',
           values: [
             {
               value: ''
             },
             {
               value: 'Officiel pas en premier',
               label: 'Officiel pas en premier'
             }, {
               value: 'Meilleur pas en premier',
               label: 'Meilleur pas en premier'
             }, {
               value: 'Officiel absent', label: 'Officiel absent'
             }]
         },
         '2' : { html: 'input', type: 'text', width: '100%' },
         '3' : { html: '', type: null, width: '100%' },
         '4' : { html: '', type: null, width: '100%' },
         '5' : { html: '', type: null, width: '100%' },
         '6' : { html: '', type: null, width: '100%' },
         '7' : { html: '', type: null, width: '100%' },
         '8' : { html: '', type: null, width: '100%' }
       });

     // vm.dtOptions = {
     //     dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
     //     pagingType: 'simple',
     //     // autoWidth : false,
     //     responsive: true
     // };

     vm.isOfficialPricePresent = function(prices) {
       var prices_copy = _.isArray(prices) ? prices : [];
       var officialPresent = _.find(prices_copy, function(price) {
         return price.name.match(/official/i);
       });
       if(_.isUndefined(officialPresent)) {
         // $scope.officialNotPresent += 1;
         return false;
       }
       return true;
     };

     vm.isBestPriceFirst = function(prices) {
       var min = 9999999;
       var idx = 0;
       var i   = 0;
       var prices_copy = _.isArray(prices) ? prices : [];
       if(prices_copy.length == 0) { return false; }
       // console.log(prices_copy);
       _.each(prices_copy, function(price) {
         var value = _.parseInt(price.price);
         if(value < min) {
           min = value;
           idx = i;
         }
         i += 1;
       });
       if(idx > 0) {
         // $scope.bestNotFirst += 1;
         return false;
       }
       return true;
     };

     vm.isOfficialPriceFirst  = function(prices) {
       var prices_copy = _.isArray(prices) ? prices : [];
       var first = _.find(prices_copy, function(price) {
         return price.position == 1 && !_.isEmpty(price.name.match(/official/i));
       });
       if(_.isEmpty(first)) {
         // $scope.officialNotFirst += 1;
         return false;
       }
       return true
     };

     vm.exportCSV = function() {
       $scope.traceDebug('into export');
       var csvdata = vm.getCSVHeaders() + vm.getCSVData();
       var filename = vm.getCSVFilename();
       $scope.traceDebug(csvdata);
       $scope.traceDebug(filename);
       var blob = new Blob([csvdata],
                           { type: "application/csv;charset=utf-8" });
       FileSaver.saveAs(blob, filename);
     };

     vm.getCSVData = function() {
       var body    = '';
       $scope.traceDebug("campaign  prices " +
                         JSON.stringify($scope.currentCampaign.prices));
       _.forEach($scope.currentCampaign.prices, function(hotel) {
         body += ($scope.currentCampaign.veille || 'n/a') + ";";
         body += hotel.error + ';';
         body +=  vm.isOfficialPricePresent(hotel.prices) + ";";
         body +=  vm.isOfficialPriceFirst(hotel.prices)   + ";";
         body +=  vm.isBestPriceFirst(hotel.prices)       + ";";
         body += hotel.hotel + ";";
         // body += hotel.ts + ";";
         // body +=  $filter('formatNom')(hotel.nom) + ";";
         body +=  $filter('timeStampFormatter')(hotel.ts) + ";";
         body +=  (hotel.country || 'n/a') + ";";
         body += (hotel.market || 'n/a') + ';';
         body += 'du '+ $filter('dateFormatter')($scope.currentCampaign.fromSlot);
         body += ' ';
         body += 'au ' + $filter('dateFormatter')($scope.currentCampaign.toSlot);
         body += ';';
         var cpt = 0;
         body += _.join(_.map(hotel.prices, function(price) {
           cpt += 1;
           return cpt  + ';' + price.name + ';' + price.price;
         }), ";");
         body += "\n";
       });

       $scope.traceDebug(body);
       return body;
     };

     vm.getCSVHeaders = function() {
       var headers = 'veille;erreur;officiel present;officiel en premier;';
       headers    += 'meilleur prix en premier;nom;';
       headers    += 'date de relevé;pays;marches;slots;';

       var max_nbr_prices = -1;
       _.forEach($scope.currentCampaign.prices, function(hotel) {
         var prices_copy = _.isArray(hotel.prices) ? hotel.prices : [];
         if(!_.isEmpty(prices_copy) && prices_copy.length > max_nbr_prices) {
           max_nbr_prices = hotel.prices.length;
         }
       });

       for(var price = 1; price <= max_nbr_prices; price += 1) {
         headers += 'Position ' + price + ";";
         headers += 'Site ' + price + ";";
         headers += 'Prix ' + price + ";";
       }
       headers += "\n";
       $scope.traceDebug(headers);
       return headers;
     };

     vm.getCSVFilename = function() {
       var outfile = 'du_' + moment($scope.currentCampaign.fromSlot).format('DDMMYYYY')
           + '_au_' + moment($scope.currentCampaign.toSlot).format('DDMMYYYY') + '_le_';
       outfile += moment().format("DD-MM-YY_[a]_HH[h]mm") + '.csv';
       $scope.traceDebug("out file " + outfile);
       return outfile;
     };

     // we have an id  for this consultation
     vm.onCampaignCreateComplete = function(data) {
       // blockUI.message("Creation de la consultation...OK");
       $scope.showProgress = false;
       $scope.traceDebug("success create campaign " + JSON.stringify(data));
       $scope.newCampaign = new Campaign({markets: [Markets[0]],
                                          proxies: [Countries[0]],
                                          hotels:  [Hotels[0]]
                                         });
       $scope.traceDebug("success new campaign " + JSON.stringify($scope.newCampaign));
       // $scope.$digest();
       // $scope.campaign =
       $scope.campaigns.push(angular.copy(data));
       // blockUI.stop();
       // $scope.publish(events.message._CHECK_PROGRESS_START_);
     };

     vm.onCampaignRunComplete = function(campaignId) {
       $scope.traceDebug('comapaing run complete <' + campaignId + '>');
       $scope.publish(events.message._GET_PRICE_BY_CAMPAIGN_ID_,
                      [campaignId]);

     };

     vm.onCampaignCreateFailed = function(data) {
       $scope.traceDebug('failed create campaign ' + data.msg);
       $scope.isConsultRun = false;
       $scope.showProgress = false;
       // blockUI.stop();
       $mdToast.show($mdToast.simple()
                     .position('top right')
                     .highlightClass('md-warn')
                     .highlightAction(true)
                     .hideDelay(0)
                     .action('OK')
                     .textContent(data.msg));
     };

     vm.onCheckProgressFailed = function(data) {
       $scope.traceDebug('failed check progress ' + JSON.stringify(data));
       var campaignId = data.campaignId;
       // _.remove($scope.campaigns, function(item) {
       //     return item.id == campaignId;
       // });
       // $mdToast.show($mdToast.simple()
       //               .position('top right')
       //               .highlightClass('md-warn')
       //               .highlightAction(true)
       //               .hideDelay(1000)
       //               .textContent(data.error));
       $scope.traceDebug('check progress Restart ');
       $scope.publish(events.message._CHECK_PROGRESS_START_);
       // blockUI.stop();
     };
     $scope.onConsult = function() {
       // blockUI.start("Creation de la consultation...");
       // $scope.traceDebug('event msg ' + events.message._CAMPAIGN_CREATE_);
       // $scope.traceDebug('event msg ' + $scope.selectedIndex);
       $scope.isConsultRun = true;
       $scope.disablebuttons = true;
       $scope.showProgress = true;
       // $scope.campaign.id     = '';
       // $scope.campaign.prices = [];
       // $scope.traceDebug("extended camparams " + JSON.stringify($scope.campaign));
       // var camp = new Campaign();
       // angular.extend(camp, camparams);
       // $scope.traceDebug("extended camp " + JSON.stringify(camp));
       $scope.publish(events.message._CAMPAIGN_CREATE_, [$scope.newCampaign]);
     };

     vm.consultComplete = function(campaignId) {
       //     $scope.disablebuttons = false;
       //     $scope.isConsultRun  = false;
       //     $scope.campaign     = new Campaign();
       $scope.publish(events.message._GET_PRICE_BY_CAMPAIGN_ID_,
                      [campaignId]);
     };

     vm.getPriceByCampaignIdFailed = function(data) {
       $mdToast.show($mdToast.simple()
                     .position('top right')
                     .highlightClass('md-warn')
                     .highlightAction(true)
                     .hideDelay(1000)
                     .action('OK')
                     .textContent('Erreur: Liste des prix indisponible'));
       $scope.showProgress = false;
     };
     vm.getPriceByCampaignIdComplete = function(data) {
       // blockUI.stop();
       // $scope.disablebuttons = false;
       // $scope.isConsultRun  = false;
       $scope.showProgress = false;
       $scope.officialNotFirst =  0;
       $scope.officialNotPresent = 0;
       $scope.bestNotFirst = 0;

       $scope.currentCampaign = _.find($scope.campaigns, { id: data.campaignId });
       // $scope.campaign.id });
       _.each(data.prices, function(price) {
         if(!vm.isOfficialPriceFirst(price.prices)) {
           $scope.officialNotFirst += 1;
         }
         if(!vm.isOfficialPricePresent(price.prices)) {
           $scope.officialNotPresent += 1;
         }
         if(!vm.isBestPriceFirst(price.prices)) {
           $scope.bestNotFirst += 1;
         }
       });

       $scope.isOfficialFirstGloable = $scope.officialNotFirst === 0 ? true : false
       $scope.currentCampaign.prices = data.prices;
       // $scope.campaign.prices = data;
       // $scope.traceDebug("current campaign show price " +
       //                   JSON.stringify($scope.currentCampaign));
       pricesCache.put('latest', data);
       // $scope.campaign = new Campaign({markets: [Markets[0]],
       //                                 proxies: [Countries[0]],
       //                                 // id:      data.id,
       //                                 hotels:  [Hotels[0]]
       //                                });

       // $scope.campaign = new Campaign();
       // $scope.traceDebug('get prices data APRES ' +
       //                   JSON.stringify($scope.campaign));

       $scope.selectedIndex = 1;
     };

     vm.cancelRun = function() {
       $scope.traceDebug('cancel run');
       $scope.disablebuttons = false;
       $scope.isConsultRun  = false;
       $scope.showProgressBar = false;
     };

     vm.onDestroyCampaignComplete = function(campaignId) {
       $scope.traceDebug("remove campaign " + campaignId);
       _.remove($scope.campaigns, function(c) {
         return c.id == campaignId;
       });
     };

     $scope.subscribe(events.message._CONSULT_COMPLETE_,
                      vm.consultComplete);

     $scope.subscribe(events.message._CAMPAIGN_RUN_CANCEL_,
                      vm.cancelRun);

     $scope.subscribe(events.message._GET_PRICE_BY_CAMPAIGN_ID_COMPLETE_,
                      vm.getPriceByCampaignIdComplete);
     $scope.subscribe(events.message._GET_PRICE_BY_CAMPAIGN_ID_FAILED_,
                      vm.getPriceByCampaignIdFailed);


     $scope.subscribe(events.message._CAMPAIGN_CREATE_COMPLETE_,
                      vm.onCampaignCreateComplete);
     $scope.subscribe(events.message._CAMPAIGN_CREATE_FAILED_,
                      vm.onCampaignCreateFailed);
     $scope.subscribe(events.message._CAMPAIGN_RUN_COMPLETE_,
                      vm.onCampaignRunComplete);
     $scope.subscribe(events.message._DESTROY_CAMPAIGN_COMPLETE_,
                      vm.onDestroyCampaignComplete);

     $scope.subscribe(events.message._CHECK_PROGRESS_ERROR_,
                      vm.onCheckProgressFailed);
   }

 })();
