(function ()
 {
     'use strict';

     /** ngInject */
     function ConsultTableCtrl($controller, $scope, events, Campaign, $mdDialog,
                               DTOptionsBuilder, api, checkprogress) {
         $controller('BaseCtrl', { $scope: $scope});
         $scope.name = 'ConsultTableCtrl';
         var vm = this;
         // vm.showSpinner = false;
         vm.dtInstance = {};
         vm.dtOptions = {
             dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
             pagingType: 'simple',
             autoWidth : false,
             responsive: true
         };

         // vm.dtOptions = DTOptionsBuilder.newOptions()
         //     .withOption('order', [3, 'desc']);


         // TODO: optimize if needed
         vm.onCheckProgressData = function(data) {
             $scope.traceDebug("checkprogress data " + JSON.stringify(data));
             if(!_.isEmpty(data)) {
                 var ids = _.map(data, _.property('id'));
                 _.each(data, function(progress) {
                     var id       = progress.id;
                     var status   = progress.status;
                     var value    = progress.progress;
                     var campaign = _.find(vm.campaignsData, { id: id });
                     if(!_.isEmpty(campaign))  {
                         campaign.status = status;
                         campaign.progress = value;
                     }
                 });
                 // TODO: safe digest
                 $scope.$digest();
             }
         };

         // $scope.publish(events.message._DESTROY_CAMPAIGN_, [campaignId]);
         $scope.subscribe(events.message._CHECK_PROGRESS_DATA_,
                          vm.onCheckProgressData);
         vm.$onInit = function() {
             $scope.traceDebug('initialize campaigns tables');
             $scope.traceDebug('vm.campaignsData ' + JSON.stringify(vm.campaignsData));
             checkprogress.start();
         };

         vm.$onDestroy = function() {
             $scope.traceDebug('destroy campaigns tables');
             // $scope.traceDebug('vm.campaignsData ' +
             // JSON.stringify(vm.campaignsData));
             checkprogress.stopChecking();
         };

         vm.onShowPrices = function(campaign) {
             vm.showSpinner = true;
             $scope.traceDebug('on show prices '+campaign.id);
             vm.showPrices({value: campaign});
         };
         vm.deleteCampaign = function(idx) {
             var campaignToDelete = vm.campaignsData[idx];
             $scope.traceDebug('delete camapign' + campaignToDelete.id);
             vm.onRemoveCampaignConfirm({value: campaignToDelete.id});
             vm.dtInstance.reloadData();
         };

     }


     angular
         .module('app.consult')
         .component('consultTable', {
             templateUrl: 'app/main/apps/consult/consult-table.html',
             bindings: {
                 campaignsData: '<',
                 showPrices: '&',
                 refreshTestsList: '&',
                 onRemoveCampaignConfirm: '&'
             },
             controller: ConsultTableCtrl
         })
         .config(config);


     /** @ngInject */
     function config()
     {
     }

 })();
