(function ()
 {
     'use strict';

     angular
         .module('app.consult', ['app.core', 'app', 'app.models', 'ngFileSaver',
                                 'datatables', 'datatables.light-columnfilter'])
         .config(config);

     /** @ngInject */
     // angular.module('app.consult').run(run);

     /** @ngInject */
     function run(checkprogress) {
         checkprogress.start();
     }

     function config($stateProvider, msNavigationServiceProvider)
     {
         // State
         $stateProvider.state('app.consult', {
             url      : '/consultation',
             views    : {
                 'content@app': {
                     templateUrl: 'app/main/apps/consult/consult.html',
                     controller : 'ConsultCtrl as vm'
                     // resolve:  {
                     //     campaigns: function(campaignsCache, api) {
                     //         return api.campaign.all();
                     //         // return campaignsCache.all();
                     //     }
                     // }
                 }
             }
         });
         // Navigation
         msNavigationServiceProvider.saveItem('apps', {
             title : 'Navigation',
             group : true,
             weight: 1
         });

         // Navigation
         msNavigationServiceProvider.saveItem('apps.consult', {
             title : 'Consultation',
             icon  : 'icon-flashlight',
             state : 'app.consult',
             weight: 1
         });
     }

 })();
