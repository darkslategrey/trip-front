(function ()
 {
     'use strict';

     angular
         .module('app.faq', ['app.core', 'app'])
         .config(config);

     function config($stateProvider, msNavigationServiceProvider)
     {
         // State
         $stateProvider.state('app.faq', {
             url      : '/faq',
             views    : {
                 'content@app': {
                     templateUrl: 'app/main/apps/faq/faq.html',
                     controller : 'FaqCtrl as vm'
                 }
             }
         });
         // Navigation
         msNavigationServiceProvider.saveItem('apps', {
             title : 'Navigation',
             group : true,
             weight: 1
         });

         // Navigation
         msNavigationServiceProvider.saveItem('apps.faq', {
             title : 'FAQ',
             icon  : 'icon-help',
             state : 'app.faq',
             weight: 5
         });
     }

 })();
