describe('Controller: Notifs', function () {
    var $scope, $rootScope, $controller;

    it('Expect True to be True', function () {
        expect(true).toBe(true);
    });


    beforeEach(function () {
        // module('app');
        module('app.notifs');
        module('app.models');
        module('generatorGulpAngular');
    });

    var $compile, $componentController, $state, $templateCache,
        template, Campaign;

    beforeEach(inject(function (_$controller_, _$rootScope_, _$compile_,
                                _$componentController_, _$state_,
                                _$templateCache_, _Campaign_) {
        $rootScope  = _$rootScope_;
        $controller = _$controller_;
        $compile    = _$compile_;

        Campaign = _Campaign_;

        $componentController = _$componentController_;
        $templateCache = _$templateCache_;
        $state         = _$state_;

        template = $templateCache.get('app/main/apps/notifs/notifs.html');
        template = angular.element(template);
    }));


    it('can be instantiated', function() {
        $scope = $rootScope.$new();
        var ctrl =  $controller('NotifsCtrl', { $scope: $scope });
        expect(ctrl).not.toBe(null);
    });


    it('must have boolean set to false', function() {
        $scope = $rootScope.$new();
        var ctrl =  $controller('NotifsCtrl', { $scope: $scope });
        expect($scope.officialPriceMissing).toBe(true);
    });

    it('should to to the state', function() {
        $state.go('app.notifs');
        $rootScope.$digest();
        expect($state.current.name).toBe('app.notifs');
    });

    it('should digest', function(){
        var $scope = $rootScope.$new();
        $scope.campaign = new Campaign();
        $compile(template)($scope);
        $scope.$digest();
    });
});



