(function () {

    'use strict';

    angular.module('app.notifs')
        .controller('NotifFormDialogCtrl', NotifFormDialogCtrl);

    /** @ngInject */
    function NotifFormDialogCtrl($mdDialog, dialogData, $controller, $scope, Notif) {
        $controller('BaseCtrl', {$scope: $scope});
        var vm = this;
        $scope.name = 'NotifFormDialogCtrl';

        vm.dialogData = dialogData;
        vm.isConsultRun = false;
        vm.disablebuttons = true;

        // Methods
        vm.saveNotif = saveNotif;
        vm.closeDialog = closeDialog;

        init();

        function init()
        {
            vm.dialogTitle = (vm.dialogData.type === 'add' ? 'Ajouter une Notif' : 'Modifier une Notif');
            $scope.traceDebug('campaign from notif form ' +
                              JSON.stringify(dialogData.campaign));
            vm.campaign = dialogData.campaign;
            // Edit
            if ( vm.dialogData.notif )
            {
                // Clone the calendarEvent object before doing anything
                // to make sure we are not going to brake the Full Calendar
                vm.notif = angular.copy(vm.dialogData.notif);
            }
            // Add
            else
            {
                vm.notif= new Notif();
            }
            $scope.traceDebug("notif name " + vm.notif.name);
        }

        function saveNotif()
        {
            var response = {
                type         : 'add',
                notif: vm.notif
            };
            $mdDialog.hide(response);
        }
        /**
         * Close the dialog
         */
        function closeDialog()
        {
            $mdDialog.cancel();
        }
    }

}());
