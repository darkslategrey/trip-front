(function ()
 {
     'use strict';

     angular
         .module('app.notifs', ['app.core', 'app', 'app.models'])
         .config(config);

     /** @ngInject */
     function config($stateProvider, msNavigationServiceProvider)
     {
         // State
         $stateProvider.state('app.notifs', {
             url      : '/notifications',
             views    : {
                 'content@app': {
                     templateUrl: 'app/main/apps/notifs/notifs.html',
                     controller : 'NotifsCtrl as vm'
                 }
             },
             params: {
                 campaign: null
             }
         });

         // Navigation
         msNavigationServiceProvider.saveItem('apps.notifs', {
             title : 'Notifications',
             icon  : 'icon-alert-box',
             state : 'app.notifs',
             weight: 2
         });
     }

 })();
