(function ()
 {
     'use strict';

     angular
         .module('app.notifs')
         .controller('NotifsCtrl', NotifsCtrl);

     /** @ngInject */
     function NotifsCtrl($scope, $controller, Campaign, $stateParams,
                         Markets, Countries, Hotels,
                         $mdDialog, $document)
     {
         $controller('BaseCtrl', {$scope: $scope});
         $scope.name = 'notifsctrl';


         var vm = this;

         if(_.isEmpty($stateParams.campaign)) {
             $scope.campaign = new Campaign();
             $scope.campaign.markets = [Markets[0]];
             $scope.campaign.hotels = [Hotels[0]];
             $scope.campaign.proxies = [Countries[0]];
         } else {
             $scope.traceDebug('proxies avant ' + JSON.stringify($stateParams.campaign.proxies));
             $scope.traceDebug('markets avant ' + JSON.stringify($stateParams.campaign.markets));
             $scope.traceDebug('hotels avant ' + JSON.stringify($stateParams.campaign.hotels));
             $scope.campaign = new Campaign($stateParams.campaign);
         };

         $scope.traceDebug('proxies ' + JSON.stringify($scope.campaign.proxies));
         $scope.traceDebug('markets ' + JSON.stringify($scope.campaign.markets));
         $scope.traceDebug('hotels ' + JSON.stringify($scope.campaign.hotels));

         $scope.isConsultRun = false;
         $scope.disablebuttons = true;
         $scope.showProgress = false;
         $scope.disablePrices   = true;
         $scope.disableConsult   = true;


         $scope.officialPriceMissing  = true;
         $scope.bestPriceNotFirst     = true;
         $scope.officialPriceNotFirst = true;

         $scope.onCreate = function() {
             $scope.traceDebug('into consult');
         };

         vm.addNotif = function(e, type, notif) {
             showEventFormDialog(type, notif, e);
         };

         vm.notifs = [];

         function showEventFormDialog(type, notif, e)
         {
             $scope.traceDebug('campaign scop ' + JSON.stringify($scope.campaign));
             $scope.traceDebug('here the type ' + type);
             var dialogData = {
                 type         : type,
                 notif       : notif,
                 isConsultRun : "isConsultRun",
                 disablebuttons: "disablebuttons",
                 campaign: $scope.campaign
             };

             $mdDialog.show({
                 controller         : 'NotifFormDialogCtrl',
                 controllerAs       : 'vm',
                 templateUrl        : 'app/main/apps/notifs/dialogs/details.html',
                 parent             : angular.element($document.body),
                 targetEvent        : e,
                 clickOutsideToClose: true,
                 locals             : {
                     dialogData: dialogData
                 }
             }).then(function (response) {
                 $scope.traceDebug('response notif ' + JSON.stringify(response.notif));
                 vm.notifs.push(response.notif);
             });
         }

     }

 })();
