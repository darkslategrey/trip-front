(function () {

    'use strict';

    angular.module('app.veille')
        .controller('NotifFormDialogCtrl', NotifFormDialogCtrl);

    /** @ngInject */
    function NotifFormDialogCtrl($mdDialog, dialogData, $controller, $scope,
                                 Notif) {
        $controller('BaseCtrl', {$scope: $scope});
        var vm = this;

        vm.dialogData = dialogData;
        $scope.name = 'NotifFormDialogCtrl';
        // Methods
        vm.saveNotif = saveNotif;
        vm.closeDialog = closeDialog;

        init();

        /**
         * Initialize
         */
        function init()
        {
            vm.dialogTitle = (vm.dialogData.type === 'add' ? 'Ajouter une Notif' : 'Modifier une Notif');

            // Edit // test
            if ( vm.dialogData.notif )
            {
                // Clone the calendarEvent object before doing anything
                // to make sure we are not going to brake the Full Calendar
                vm.dest = _.join(vm.dialogData.notif.dest, "\n");
                vm.notif = new Notif(vm.dialogData.notif);
            }
            // Add
            else
            {
                vm.notif= new Notif();
            }
            $scope.traceDebug("veill name " + vm.notif.name);
        }
        function saveNotif()
        {
            vm.notif.dest = _.split(vm.dest, "\n");
            var response = {
                type         : 'add',
                notif: vm.notif
            };
            $mdDialog.hide(response);
        }

        /**
         * Close the dialog
         */
        function closeDialog()
        {
            $mdDialog.cancel();
        }
    }

}());
