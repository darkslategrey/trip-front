(function () {

    'use strict';

    angular.module('app.veille')
        .controller('VeilleFormDialogCtrl', VeilleFormDialogCtrl);

    angular.module('app.veille')
        .filter('notifDestFormatter', NotifDestFormatter);

    function NotifDestFormatter($sce) {
        return function(dests) {
            var out = _.join(dests, "<br/>");
            return $sce.trustAsHtml(out);
        };
    }

    /** @ngInject */
    function VeilleFormDialogCtrl($mdDialog, dialogData, $controller,
                                  $document, $scope, Veille) {
        $controller('BaseCtrl', {$scope: $scope});
        var vm = this;

        vm.dialogData = dialogData;
        $scope.name = 'VeilleFormDialogCtrl';

        // Methods
        vm.saveVeille = saveVeille;
        vm.closeDialog = closeDialog;

        vm.selectedNotif = undefined;

        $scope.nbrOfHours = ['8 heures', '12 heures', '24 heures'];
        $scope.nbrOfTU = [1, 2, 3, 4, 5, 6];
        $scope.timeUnits = ['Jours', 'Semaines', 'Mois'];

        init();

        $scope.dtOptions = {
            paging:   false,
            ordering: false,
            info:     false,
            searching: false
        };

        $scope.isRelatifs = vm.veille.slots_type == 'relatifs';

        vm.changeSlotsType = function() {
            $scope.traceDebug(" change value " + vm.veille.slots_type);
            $scope.isRelatifs = vm.veille.slots_type == 'relatifs';
        };

        vm.deleteNotifConfirm = function(notif, ev, form)
        {
            var confirm = $mdDialog.confirm({skipHide: true})
                .title('Êtes-vous sûr ?')
                .ariaLabel('Supprimer la notification')
                .targetEvent(ev)
                .ok('OK')
                .cancel('Annuler');

            $mdDialog.show(confirm).then(function () {
                vm.deleteNotif(notif);
                form.$setDirty();
            }, function () {});
        };

        vm.deleteNotif = function(notif) {
            vm.veille.notifs = _.reject(vm.veille.notifs, function(n) {
                return _.isEqual(notif, n);
            });
        };

        //////////

        /**
         * Initialize
         */
        function init()
        {
            $scope.dialogTitle = (vm.dialogData.type === 'add' ? 'Ajouter une Veille' : 'Modifier une Veille');
            if ( vm.dialogData.veille ) {
                // Edit
                $scope.traceDebug("go to edit");
                vm.veille = new Veille(vm.dialogData.veille);
            } else {
                // Add
                $scope.traceDebug("go to create");
                vm.veille= new Veille();
            }
            $scope.traceDebug("veill name " + vm.veille.name);
        }

        vm.addNotif =  function(e, type, notif, form) {
            $scope.traceDebug('add notif');
            if(type == 'edit') {
                vm.selectedNotifIdx = _.findIndex(vm.veille.notifs, notif);
                $scope.traceDebug('save selectedNotif idx ' + vm.selectedNotifIdx);
            }
            showNotifDialog(type, notif, e, form);
        };

        function showNotifDialog(type, notif, e, form)
        {
            var parent = angular.element(document.querySelector('#veilleDialog'));
            var dialogData = {
                type         : type,
                notif       : notif
            };
            // parent             : ele,
            // parent             : angular.element($document.body),
            $mdDialog.show({
                skipHide           : true,
                controller         : 'NotifFormDialogCtrl',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/veille/dialogs/notif.html',
                parent             : parent,
                targetEvent        : e,
                clickOutsideToClose: true,
                locals             : {
                    dialogData: dialogData
                }
            }).then(function (response) {
                $scope.traceDebug('response notif ' + JSON.stringify(response.notif));
                if(type == 'add') {
                    vm.veille.notifs.push(response.notif);
                } else {
                    $scope.traceDebug('readey to change notif value');
                    vm.veille.notifs[vm.selectedNotifIdx] = response.notif;
                }
                form.$setDirty();
            });
        }

        function saveVeille()
        {
            var response = {
                type: vm.dialogData.type,
                veille: vm.veille
            };
            $mdDialog.hide(response);
        }
        /**
         * Close the dialog
         */
        function closeDialog()
        {
            $mdDialog.cancel();
        }
    }

}());
