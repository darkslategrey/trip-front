(function ()
{
    'use strict';

    angular
        .module('app.veille')
        .controller('VeilleCtrl', VeilleCtrl);



    /** @ngInject */
    function VeilleCtrl($scope, $stateParams, $controller, Veille, $mdDialog,
                        $document, api, events, $mdToast, veilles)
    {
        $controller('BaseCtrl', {$scope: $scope});
        var vm = this;
        $scope.name = 'VeilleCtrl';
        vm.veilles = veilles;

        if(_.isEmpty($stateParams.veille)) {
            vm.veille = new Veille();
        } else {
            vm.veille = new Veille($stateParams.veille);
        };

        vm.dtOptions = {
            dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth : false,
            responsive: true
        };

        $scope.nbrOfTU   = [{ id: 1, name: 1}, { id: 2, name: 2}];
        $scope.timeUnits = [{ id: 1, name: 'jours'}, { id: 2, name: 'semaines'},
                            {id: 3, name: 'mois'}];
        $scope.isConsultRun = false;
        $scope.disablebuttons = true;
        $scope.selectedTimeUnit = 1;
        $scope.selectedNbrOfUnit = 'Jours';

        vm.formatTimeUnit = function(nbrOfUnit, timeUnit) {
            var output = nbrOfUnit + ' ' + timeUnit;
            if((timeUnit === 'Semaines' || timeUnit === 'Jours') && nbrOfUnit == 1) {
                output = nbrOfUnit + ' ' + _.replace(timeUnit, /s$/, '');
            }
            return output;
        };

        vm.removeVeilleConfirm = function(veilleId, e) {
            var confirm = $mdDialog.confirm({skipHide: true})
                .title('Êtes-vous sûr ?')
                .ariaLabel('Supprimer la veille')
                .targetEvent(e)
                .ok('OK')
                .cancel('Annuler');

            $mdDialog.show(confirm).then(function () {
                vm.removeVeille(veilleId);
            }, function () {});
        };

        vm.removeVeille = function(veilleId) {
            $scope.traceDebug("go to remove veille "+veilleId);
            $scope.publish(events.message._VEILLE_REMOVE_, [veilleId]);
        };

        vm.addVeille =  function(e, type, veille) {
            showEventFormDialog(type, veille, e);
        };

        function showEventFormDialog(type, veille, e)
        {
            $scope.traceDebug('here the type ' + type);
            var dialogData = {
                type         : type,
                veille       : veille,
                isConsultRun : "isConsultRun",
                disablebuttons: "disablebuttons"
            };

            $mdDialog.show({
                controller         : 'VeilleFormDialogCtrl',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/veille/dialogs/details.html',
                parent             : angular.element($document.body),
                targetEvent        : e,
                clickOutsideToClose: true,
                locals             : {
                    dialogData: dialogData
                }
            }).then(function (response) {
                $scope.traceDebug("close dialog " + response.type);

                if(response.type == 'add') {
                    $scope.traceDebug("go to publish veille CREATE");
                    $scope.traceDebug("veille " + JSON.stringify(response.veille));
                    vm.currentCreatedVeille = angular.copy(response.veille);
                    $scope.publish(events.message._VEILLE_CREATE_, [response.veille]);
                } else {
                    $scope.traceDebug("go to publish veille UPDATE");
                    vm.selectedVeilleIdx = _.findIndex(vm.veilles, function(veille) {
                        return veille.id == response.veille.id;
                    });
                    // vm.selectedVeilleIdx = _.findIndex(vm.veilles, response.veille);
                    vm.currentUpdatedVeille = angular.copy(response.veille);
                    $scope.traceDebug("saved idx " + vm.selectedVeilleIdx);
                    $scope.publish(events.message._VEILLE_UPDATE_, [response.veille]);
                }
            });
        }

        function onVeilleUpdateComplete() {
            $scope.traceDebug("Update veille done");
            vm.veilles[vm.selectedVeilleIdx] = vm.currentUpdatedVeille;
            $mdToast.show($mdToast.simple()
                          .position('top right')
                          .hideDelay(400)
                          .textContent("Mise a jour OK"));
        }

        function onVeilleUpdateFailed() {
            $scope.traceDebug("Update veille failed");
            $mdToast.show($mdToast.simple()
                          .position('top right')
                          .highlightClass('md-warn')
                          .highlightAction(true)
                          .hideDelay(0)
                          .action('OK')
                          .textContent("Erreur lors de la mise a jour"));
        }

        function onVeilleCreateComplete(veille) {
            $scope.traceDebug("Create veille done");
            vm.currentCreatedVeille.id = veille.id;
            vm.veilles.push(angular.copy(vm.currentCreatedVeille));
            vm.currentCreatedVeille = null;
        }

        function onVeilleRemoveComplete(veilleId) {
            $scope.traceDebug("Delete veille done for "+ veilleId);
            _.remove(vm.veilles, function(v) {
                return v.id == veilleId;
            });
            // vm.veille.notifs = _.reject(vm.veille.notifs, function(n) {
            //     return _.isEqual(notif, n);
            // });
        }

        function onVeilleCreateFailed() {
            $scope.traceDebug("Create veille failed");
            $mdToast.show($mdToast.simple()
                          .position('top right')
                          .highlightClass('md-warn')
                          .highlightAction(true)
                          .hideDelay(0)
                          .action('OK')
                          .textContent("Erreur lors de la création de la veille"));
        }

        $scope.subscribe(events.message._VEILLE_CREATE_COMPLETE_,
                         onVeilleCreateComplete);
        $scope.subscribe(events.message._VEILLE_CREATE_FAILED_,
                         onVeilleCreateFailed);
        $scope.subscribe(events.message._VEILLE_UPDATE_COMPLETE_,
                         onVeilleUpdateComplete);
        $scope.subscribe(events.message._VEILLE_UPDATE_FAILED_,
                         onVeilleUpdateFailed);
        $scope.subscribe(events.message._VEILLE_REMOVE_COMPLETE_,
                         onVeilleRemoveComplete);
    }
})();
