(function ()
 {
     'use strict';

     angular
         .module('app.veille', ['app.models'])
         .config(config);

     /** @ngInject */
     function config($stateProvider, msNavigationServiceProvider)
     {
         // State
         $stateProvider.state('app.veille', {
             url      : '/veille',
             views    : {
                 'content@app': {
                     templateUrl: 'app/main/apps/veille/veille.html',
                     controller : 'VeilleCtrl as vm',
                     resolve:  {
                         veilles: function(veillesCache, api) {
                             return veillesCache.all();
                         }
                     }
                 }
             },
             params: {
                 veille: null
             }
         });

         // Navigation
         msNavigationServiceProvider.saveItem('apps.veille', {
             title : 'Veille',
             icon  : 'icon-eye',
             state : 'app.veille',
             weight: 4
         });
         
     }

 })();
