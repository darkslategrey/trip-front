
describe('global', function() {
    var log;

    beforeAll(inject(function(_$log_) {
        log = _$log_;
    }));

    afterEach(function() {
        console.log(log.debug.logs);
        console.log(log.log.logs);
        console.log(log.error.logs);
    });

        // $httpBackend.verifyNoOutstandingExpectation();
        // $httpBackend.verifyNoOutstandingRequest();
});
