(function ()
 {
   'use strict';

   angular.module('app', ['fuse']);
   angular.module('app.models', []);
   angular.module('app.cacheservice', []);
   angular.module('app.test', []);
   angular.module('app.values', [])
     .value('Markets',   [{name: 'Tous', id: 'all'},
                          {id: 1, name: 'mobile'}, {id: 2, name: 'desktop'}])
     .value('Countries', [
       {country_code: 'all', country_name: "Tous" },
       {country_code: "fr", country_name: "France" },
       {country_code: 'au', country_name: 'Australie'},
       {country_code: 'ch', country_name: 'Suisse'},
       {country_code: "us", country_name: "USA" },
       {country_code: "gb", country_name: "UK" },
       {country_code: "de", country_name: "Allemagne" },
       {country_code: "es", country_name: "Espagne" },
       {country_code: "it", country_name: "Italie" },
       {country_code: "pr", country_name: "Portugal" },
       {country_code: "ru", country_name: "Russie" },
       {country_code: "jp", country_name: "Japon" },
       {country_code: "cn", country_name: "Chine" },
       {country_code: "ca", country_name: "Canada" },
       {country_code: "br", country_name: "Bresil" },
       {country_code: "in", country_name: "Inde" },
       {country_code: "ko", country_name: "Coree" },
       {country_code: "il", country_name: "Israël" }
     ])
     .value('Hotels',    [
       {name: 'Tous', id: 'all'},
       {name: '34B', id: 9566832},
       {name: 'Acadia', id: 229969},
       {name: 'Astoria', id: 230431},
       {name: 'Astra Opera', id: 229968},
       {name: 'Augustin', id: 197551},
       {name: 'Bradford Elysees', id: 197946},
       {name: 'Caumartin Opera', id: 228778},
       {name: 'George', id: 234640},
       {name: 'Joke', id: 287896},
       {name: 'Joyce', id: 1631156},
       {name: 'Le 123 Elysees', id: 228783},
       {name: 'Le 123 Sebastopol', id: 4800782},
       {name: 'Lorette', id: 228848},
       {name: 'Malte', id: 228694},
       {name: 'Monterosa', id: 313060},
       {name: 'Palm', id: 232480}
     ]);

   angular.module('app.dataservices', []).run(function(campaignDataService,
                                                       veilleDataService,
                                                       proxyDataService) {
     campaignDataService.init();
     veilleDataService.init();
     proxyDataService.init();
   });

   /**
    * Main module of the Fuse
    */
   angular
     .module('fuse', [

       'restangular',
       'environment',
       // Root
       'app',

       // Models
       'app.models',
       'app.dataservices',
       'app.cacheservice',
       // Core
       'app.core',

       // Navigation
       'app.navigation',

       // Toolbar
       'app.toolbar',

       // Apps
       'app.consult',
       'app.faq',
       // 'app.notifs',
       'app.proxies',
       'app.veille',

       // components
       'app.components'
     ]);
 })();
