# coding: utf-8
require 'sinatra'
require 'json'
require_relative 'fixtures'

use Rack::Logger

# use to test the front
# TODO: replace this server by specs
before do
  content_type :json
  headers 'Access-Control-Allow-Origin' => '*',
          'Access-Control-Allow-Methods' => "HEAD,GET,PUT,DELETE,OPTIONS,PATCH",
          'Access-Control-Allow-Headers' => "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept"
end
# ['OPTIONS', 'GET', 'POST', 'PUT', 'DELETE'],
# 'Access-Control-Allow-Headers' => 'Content-Type'
cpt = 0

set :protection, false

options "*" do
  response.headers["Allow"] = "HEAD,GET,PUT,DELETE,OPTIONS"

  # Needed for AngularJS
  response.headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept"

  200 # halt HTTP_STATUS_OK
end

options '/api/veilles' do
  200
end
options '/api/veilles/update' do
  200
end

# options '/api/veilles/:id' do
#   200
# end

get '/api/veilles' do
  BLFixtures::VeillesWithNotif.to_json
end

patch '/api/veilles/:id' do
  logger.info("update veille <#{params}>")
  # status 500
  body({msg: 'nok'}.to_json)

  # body({data: { msg: 'nook'}}).to_json
  body({data: { msg: 'ok'}}).to_json
end

post '/api/veilles' do
  logger.info("create veille <#{params}>")
  begin
    params.merge! JSON.parse(request.env["rack.input"].read)
  rescue JSON::ParserError
    logger.error "Cannot parse request body."
  end
  # sleep 3
  status 400
  params[:id] = rand 90999
  body({ data: {id: rand(90999) }}.to_json)
end

delete '/api/veilles/:id' do
  logger.info("delete veille #{params}")
  body({data: { veilleId: params[:id], msg: 'ok'}}).to_json
end

get '/api/proxies' do
  json_data = IO.read("#{File.dirname(__FILE__)}/proxify_proxies.json")
  proxies = JSON.parse(json_data)
  proxies.to_json
  # status 500
  # body({ msg: 'nok '}).to_json
  # { data: { proxies: proxies}}.to_json
end

post '/api/proxies/update_one' do
  logger.info("update proxy #{params}")
  body({data: { msg: 'ok' }}).to_json
end

post '/api/proxies/create_one' do
  logger.info("create proxy #{params}")
  body({data: { msg: 'ok', id: 1 }}).to_json
end

post '/api/proxies/remove_one' do
  data = JSON.parse request.body.read
  "Hello #{data['name']}!"
  logger.info("remove proxy #{data}")
  body({data: { msg: 'ok', id: data['id'] }}).to_json
end

post '/api/proxies/update' do
  logger.info("put veille #{params}")
  body({data: { msg: 'ok'}}).to_json
  # status 500
  # body({data: { msg: 'nook'}}).to_json
end

post '/api/veilles/update' do
  # params.merge! JSON.parse(request.env["rack.input"].read)
  logger.info("put veille #{params}")
  body({data: { msg: 'ok'}}).to_json
end

options '/api/prices' do
  200
end

# best prices not first

# prices = inputs.map { |input|
#   {
#     id: input.origin_id, hotel: input.origin_name,
#     prices: input.data, market: 'desktop', countries: ['Fr'],
#     campaign_id: campaign_id
#   }
# }

best_not_first = {id: 1, hotel: 'h1', market: 'm1',
                    countries: 'tous', markets: 'tous',
                  prices: { pprices: [{official: 5}, {e: 4}, {f: 6}]},
                  campaign_id: :campaign_id}
official_not_present = {id: 2, hotel: 'h2', market: 'm1',
                        countries: 'tous', markets: 'tous',
                        prices: [{e: 5}, {f: 6}],
                        campaign_id: :campaign_id}
official_not_first  =  {id: 3, hotel: 'h3', market: 'm1',
                        countries: 'tous', markets: 'tous',
                        prices: [{e: 5}, {f: 6}, {official: 5}],
                        campaign_id: :campaign_id}
best_not_first_official_not_present = {id: 4, hotel: 'h4', market: 'm1',
                                       countries: 'tous', markets: 'tous',
                                       prices: [{g: 5}, {e: 4}, {f: 6}],
                                       campaign_id: :campaign_id}
normal  =  {id: 5, hotel: 'h5', market: 'm1',
            countries: 'tous', markets: 'tous',
            prices: [{official: 3}, {f: 4}, {g: 5}],
            campaign_id: :campaign_id}

get '/api/campaigns/:campaign_id/cancel' do
  logger.info("id <#{params[:campaign_id]}> is canceled")
  { data: { msg: 'Cancel Ok' }}.to_json
end

get '/api/campaigns/:campaign_id/prices-disabled' do
  status 500
  body({data: { msg: 'nook'}}).to_json
end

get '/api/campaigns/:campaign_id/prices' do
  logger.info("id <#{params[:campaign_id]}>")
  # json_data = IO.read("#{File.dirname(__FILE__)}/hotels_prices_backoffice.json")
  json_data = IO.read("#{File.dirname(__FILE__)}/pricestest2802.json")
  prices = JSON.parse(json_data)
  prices["data"]["campaignId"] = params[:campaign_id]
  # cpt = 0
  cpt = 100
  sleep 5
  prices.to_json
  # { data: { campaignId: params[:campaign_id], releve: price}}.to_json
end

# get '/api/campaigns/:campaign_id/prices' do
#   logger.info("id <#{params[:campaign_id]}>")
#   json_data = IO.read("#{File.dirname(__FILE__)}/hotels_prices.json")
#   prices = JSON.parse(json_data)
#   price = prices[rand prices.size]
#   price["campaign_id"] = params[:campaign_id]
#   price = [price]
#   # cpt = 0
#   cpt = 100
#   { data: { campaignId: params[:campaign_id], releve: price}}.to_json
# end

check_cpt = 0
get '/api/campaigns/checkprogress' do
  check_cpt += 1

  sleep 5
  if check_cpt > 20
    check_cpt = 1
  end
  status = 'en cours'

  case check_cpt
  when 10
    status = 'terminé'
  when 20
    status = 'erreur'
  end

  logger.info("check_cpt #{check_cpt}")

  result = { result: [ { id: "589fd570b4a34d1846150045", status: 'terminé',
                         value: rand(99..100)},
                       { id: "589fda47b4a34d2f7b9e3d4c", status: 'terminé',
                         value: rand(1..100)},
                       { id: "589fdbfab4a34d36926c9229", status: 'terminé',
                         value: rand(1..100)}
                     ]}.to_json
  # result = {}.to_json
  result
end

options '/api/campaigns' do
  200
end
options '/api/notifs' do
  200
end
post '/api/notifs' do
  params.merge! JSON.parse(request.env["rack.input"].read)
  logger.info("create notifs <#{params}>")
  begin
  # params.merge! JSON.parse(request.env["rack.input"].read)
  rescue JSON::ParserError
    logger.error "Cannot parse request body."
  end
  params[:id] = 10
  { data: params, method: 'POST' }.to_json
end

post '/api/campaigns/refresh' do
  campaigns = BLFixtures::Campaigns3[0..1]
  campaigns.first[:id] = rand(10..100)
  campaigns.last[:id] = rand(10..100)
  { data: campaigns }.to_json
end

get '/api/campaigns' do
  logger.info('get all campaigns')
  # status = 200
  # status 500
  BLFixtures::Campaigns3.to_json
  # [{ error:  'campaigns not available' } ].to_json
  # [].to_json
end

post '/api/campaigns' do
  params.merge! JSON.parse(request.env["rack.input"].read)
  logger.info("create campaipng <#{params}>")
  begin
  # params.merge! JSON.parse(request.env["rack.input"].read)
  rescue JSON::ParserError
    logger.error "Cannot parse request body."
  end
  sleep 5
  # status 500
  # body({ msg: 'error create campaign'}.to_json)

  params[:id] = (rand 90999).to_s
  params[:name] = "c#{rand 1999}"
  params[:timestamp] = DateTime.now
  params[:status] = 'en cours'
  body({ data: params, method: 'POST' }.to_json)
end

delete '/api/campaigns/:id' do
  logger.info("delete veille #{params}")
  body({data: { campaignId: params[:id], msg: 'ok'}}).to_json
end

put '/api/campaigns' do
  begin
    params.merge! JSON.parse(request.env["rack.input"].read)
  rescue JSON::ParserError
    logger.error "Cannot parse request body."
  end
  { result: params[:movie], updated: true, method: 'PUT' }.to_json
end

delete '/api/campaigns' do
  begin
    params.merge! JSON.parse(request.env["rack.input"].read)
  rescue JSON::ParserError
    logger.error "Cannot parse request body."
  end
  { result: "#{params[:movie]} deleted!", method: 'DELETE' }.to_json
end
